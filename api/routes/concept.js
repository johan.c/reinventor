var express = require('express');
var router = express.Router();
var concept = require('../models/concept');
var util = require('../util/util');

var self = {
	concept : {},
	list : []
};

router.get(util.api+'/concept', function (req, res, next) {
	var query = concept.getQuery(req.query);
	util.connection.query(query, function (error, results){
		if (error) {
	      return res.send(400, {'success':false,'data' : []});
	    }else{
	      if(results.length > 0){
	        var response = self.getList(results);
	        return res.send(200, {'success':true,'data' : response});
	      }else{
	        return res.send({'success':false,'data' : []});
	      }
	    }
	});
});

self.getList = function(data){
	if(data.length > 0){
		self.list = [];
		for (var i = 0; i < data.length; i++) {
			self.list.push({
		        'id' : data[i].id,
		        'name' : data[i].name,
		        'count' : data[i].count
			});
		}
	}
	return self.list;
};

module.exports = router;