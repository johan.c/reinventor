var express = require('express');
var router = express.Router();
var mysql = require('mysql');

connection = mysql.createConnection({
				host : 'localhost',
				user : 'root',
               	password : '1234',
               	database: 'database'
			});

router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Postgis'}
  );
});

/*
router.get('/feed', function (req, res, next) {
	sitemap.toXML( function (err, xml) {
		if (err) {
			return res.status(500).end();
		}
		res.header('Content-Type', 'application/xml');
		res.send(xml);
  	});
  	//res.render('feed', {title: 'Hola Mundo'});
});
*/

router.get('/beta', function (req, res, next) {
	connection.query('SELECT * FROM dd_user', function (error, results){
		if (error) {
			return console.error('error fetching client from pool', error);
		}else{
			console.log("connected to database");
			res.send(200, results);
			return next();
		}
	});
});

module.exports = router;
