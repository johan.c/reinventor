var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var user = require('../models/user');
var api = '/api/v1/c22d6ecd-5711-2efc-2ba1-0d6d42bff682';

var self = {
	user : {},
	listUser : []
};

connection = mysql.createConnection({
	host : 'localhost',
	user : 'root',
	password : '1234',
	database: 'database'
});

router.get(api+'/user', function (req, res, next) {
	connection.query(user.query, function (error, results){
		if (error) {
			return res.send({'success':false});
		}else{
			if(results.length > 0){
				var response = self.getListUser(results);
				res.send(200, {'success':true,'data' : response});
			}else{
				return res.send({'success':false,'data' : []});
			}
			return next();
		}
	});
});

router.get(api+'/user/:id', function(req, res, next) {
  	connection.query(user.query + ' = ' + req.params.id , function (error, results){
		if (error) {
			return res.send(400,{'success':false});
		}else{
			if(results.length > 0){
				var response = self.getUser(results);
				res.send(200, {'success':true,'data' : response});
			}else{
				return res.send({'success':false,'data' : []});
			}
			return next();
		}
	});
});

/* =================== FUNCIONES DE SERVICIO ============================= */

//LISTADO DE USUARIOS
self.getListUser = function(data){
	if(data.length > 0){
		self.listUser = [];
		for (var i = 0; i < data.length; i++) {
			self.listUser.push({
				'username' : data[i].username,
				'password' : data[i].password,
				'person_id' : data[i].person_id
			});
		}
	}
	return self.listUser;
};

//USUARIO POR ID
self.getUser = function(data){
	if(data.length > 0){
		for (var i = 0; i < data.length; i++) {
			self.user = {};
			self.user.username = data[i].username,
			self.user.password = data[i].password,
			self.user.person_id = data[i].person_id
		}
	}
	return self.user;
};

/* ========================================================================= */

module.exports = router;
