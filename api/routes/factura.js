var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var document = require('../models/document');
var util = require('../util/util');
var soap = require('soap');

var log4js = require('log4js');
log4js.configure({
	appenders: {
		cheese: {
			type: 'file', 
			filename: 'cheese.log' 
		} 
	},
	categories: { 
		default: {
			appenders: ['cheese'], 
			level: 'error' 
		} 
	}
});
const logger = log4js.getLogger('cheese');

router.get('/factura', function (req, res, next) {
	var url = 'https://ws.cdyne.com/ip2geo/ip2geo.asmx?wsdl';
	var args = {
		ipAddress: '190.236.255.235',
		licenseKey: ''
	};
	soap.createClient(url, function(err, client) {
	  client.ResolveIP(args, function(err, result) {
	      //console.log(result);
	      if(err){
	      	return res.send(400, {'success':false,'data' : []});
	      }else{
	      	res.send(200, {'success':true,'data' : result});
	      }
	  });
	});
});

router.post('/document/extract', function (req, res, next) {
	//var url = 'http://facturaenuna.pe/OnlineCPE/CPE/wsOnlineToCPE.svc?wsdl'; //DEV
	var url = 'http://facturaenuna.com/OnlineCPE/CPE/wsOnlineToCPE.svc?wsdl'; //PRODUCCION
	
	var user = req.body.oUser;
	var pass = req.body.oPass;
	var nro_doc = req.body.oNroIde;

	var args = {
		"oUser": user,//util.taxtech.user
		"oPass": pass,//util.taxtech.pass
		"oNroIde": nro_doc,//util.taxtech.ruc
		"oTipCpe": req.body.oTipCpe,
		"oSerCpe": req.body.oSerCpe,
		"oNroCpe": req.body.oNroCpe,
		"oFlgXml": req.body.oFlgXml,
		"oFlgPdf": req.body.oFlgPdf,
		"oFlgCdr": req.body.oFlgCdr
	};

	soap.createClient(url, function(err, client) {
		client.callExtractCPE(args, function(err, result) {
			if(err){
				return res.send(400, {'success':false,'data' : []});
			}else{
				if(result.callExtractCPEResult !== undefined && result.hasOwnProperty("callExtractCPEResult")){
					return res.send(200, {'success':true,'data' : result.callExtractCPEResult});
				}else{
					return res.send(400, {'success':false,'data' : []});
				}
			}
		});
	});

});

router.post('/document/state', function (req, res, next) {
	var url = 'http://facturaenuna.pe/OnlineCPE/CPE/wsOnlineToCPE.svc?wsdl';
	var oTipCpe = req.body.oTipCpe; 
	var oSerCpe = req.body.oSerCpe;
	var oNroCpe = req.body.oNroCpe;

	var args = {
		"oUser": util.taxtech.user,
		"oPass": util.taxtech.pass,
		"oNroIde": util.taxtech.ruc,
		"oTipCpe": req.body.oTipCpe,
		"oSerCpe": req.body.oSerCpe,
		"oNroCpe": req.body.oNroCpe
	}

	soap.createClient(url, function(err, client) {
		client.callStateCPE(args, function(err, result) {
			if(err){
				return res.send(400, {'success':false,'data' : []});
			}else{
				if(result.callStateCPEResult !== undefined && result.hasOwnProperty("callStateCPEResult")){
					return res.send(200, {'success':true,'data' : result.callStateCPEResult});
				}else{
					return res.send(400, {'success':false,'data' : []});
				}
			}
		});
	});
});

router.post('/taxtech', function (req, res, next) {

	var nro_doc = "";
	var query = document.getMaxNro(req.body);

	logger.trace('Entering cheese testing');
	logger.debug('Got cheese.');
	logger.info('Cheese is Comté.');
	logger.warn('Cheese is quite smelly.');
	logger.error('Cheese is too ripe!');
	logger.fatal('Cheese was breeding ground for listeria.');

	//util.connection.query(query, function (error, results){
		//if (!error) {
			var url = 'http://facturaenuna.pe/OnlineCPE/CPE/wsOnlineToCPE.svc?wsdl';
			var oUser = req.body.oUser; 
			var oPass = req.body.oPass; 
			var nro_doc_emi = req.body.nro_doc_emi;
			var nom_emi = req.body.nom_emi;
			var nom_com_emi = req.body.nom_com_emi;
			
			//PARAMETROS FACTURA

			/* FECHA DE EMISION DEL COMPROBANTE */
			var fec_emi = req.body.fec_emi; 
			/*
				2018-07-27
			*/
			
			/* HORA DE EMISION DEL COMPROBANTE */
			var hor_emi = req.body.hor_emi;
			/* 
				08:56:14
			*/

			/* NUMERO DE SERIE DEL COMPROBANTE */
			var serie = req.body.serie;
			/* 
				F004
			*/

			/* NUMERO DEL CORRELATIVO DEL DOCUMENTO */
			/*
			var correlativo = 0;
			if(results[0].nro !== undefined){
				correlativo = parseInt(results[0].nro) + 1; 
			}else{
				correlativo = req.body.correlativo; 
			}

			console.log("===================");
			console.log(results[0].nro);
			console.log("===================");
			
			*/

			var correlativo = req.body.correlativo; 
			/* 
				00000020
			*/

			/* TIPO DE MONEDA */
			var moneda = req.body.moneda;
			/* 
				PEN
			*/

			/* CODIGO DE TIPO DE OPERACION */
			var cod_tip_ope = req.body.cod_tip_ope;
			/*
				0101 - Venta Interna / 
				0102 - Venta Interna - Anticipos / 
				0103 - Venta Interna - Deducción de Anticipos / 
				0104 - Venta Itinerante
			*/

			/* Código de Tipo de Identificador Fiscal del Emisor */
			var tip_doc_emi = req.body.tip_doc_emi;
			/* 
				6
			*/
			/* Número de identificador fiscal del emisor (RUC) */
			var nro_doc = req.body.nro_doc;
			/*
				20601890659
			*/

			/* Razón Social del Emisor */
			var nom_emi = req.body.nom_emi;
			/*
				TAX TECHNOLOGY PRUEBA SAC
			*/

			/* Nombre Comercial del Emisor  */
			var nom_com_emi = req.body.nom_com_emi;
			/*
				TAXTECH SAC
			*/

			/* Código de Tipo de Número de Identificador de Receptor (CLIENTE) */
			var tip_doc_rct = req.body.tip_doc_rct;
			/*
				0 – Doc. Trib. No Dom. Sin RUC.
				1 – DNI
				4 – Carnet de extranjería
				6 – RUC
				7 – Pasaporte
				A – Cedula diplomática de identidad
				B – Doc. Identi. País residencia No Domiciliado Tratándose de
				operaciones de exportación (COD_TIP_OPE = 0102) el código a
				utilizar será “-“
			*/

			/* NRO DOC DEL CLIENTE // Tratándose de operaciones de exportación (COD_TIP_OPE = 0102)  */
			var nro_doc_rct = req.body.nro_doc_rct;
			/*
				20601890000
			*/

			/* NOM CLIENTE RECEPTOR */
			var nom_rct = req.body.nom_rct;
			/*
				CLIENTE RECEPTOR S.A.C.
			*/

			/* Razón Social del Receptor (CLIENTE) */
			var dir_des_rct = req.body.dir_des_rct;
			/*
				AV. COMANDANTE ESPINAR NRO. 435 DPTO. 803 CJRES PEDRO RUIZ GALLO
			*/

			/* CODIGO DE UBIGEO DE LA PROVINCIA | DEPARTAMENTO | DISTRITO */
			var ubi_rct = req.body.ubi_rct;
			/*
				150101
			*/

			/* CODIGO DEL TIPO DE PAGO */
			var tip_pag = req.body.tip_pag;
			/*
				000 => NO ASIGNADO
				001 => CONTADO
				002 => CRÉDITO A 7 DÍAS
				003 => CRÉDITO A 15 DÍAS
				004 => CRÉDITO A 30 DÍAS
				005 => CRÉDITO A 60 DÍAS
				006 => CRÉDITO A 90 DÍAS
				007 => CRÉDITO A 120 DÍAS
				008 => CRÉDITO A 20 DÍAS
				009 => CRÉDITO A 45 DÍAS
			*/

			/* CODIGO DE FORMA DE PAGO */
			var frm_pag = req.body.frm_pag;
			/*
				000 NO ASIGNADO
				001 EFECTIVO
				002 CHEQUE
				003 LETRA
				004 TARJETA DE CRÉDITO
				005 TARJETA DE DÉBITO
				006 DEPOSITO BANCARIO
				007 TRANSFERENCIA INTERBANCARIA
			*/

			/* NUMERO DE ORDEN DE COMPRA */
			var nro_ord_com = req.body.nro_ord_com;
			/*
				OC-235466
			*/

			/* Código del Tipo de Guía Texto */
			var cod_tip_gre = req.body.cod_tip_gre;
			/*
				Tipo Texto logitud de 2
			*/ 

			/* Numero del Tipo de Guía Texto */
			var nro_gre = req.body.nro_gre;
			/*
				Tipo Texto logitud de 15
			*/ 

			/* Enviar etiqueta vacia */
			var cod_opc = req.body.cod_opc;
			/*
				Tipo Texto logitud de 50
			*/ 

			/* Forma de impresion */
			var fmt_impr = req.body.fmt;
			/*
				Tipo Texto logitud de 3, siempre enviar el valor "001"
			*/ 

			/* Impresora destino */
			var impresora = req.body.impresora;
			/*
				Tipo Texto logitud de 120, Enviar “nombre o IP del servidor”/”nombre de la impresora”
				como está configurada en la RED, siempre que la instalación sea in House. Caso contrario se envía Vacío.
			*/ 

			/* Monto descuento global en moneda de la transaccion */
			var mnt_dcto_glb = req.body.mnt_dcto_glb;
			/*
				Tipo Numerico logitud de 12.2, Distinto al elemento Total Descuentos. Su propósito es permitir
				consignar en el comprobante de pago, un descuento a nivel global o total. Este campo no debe ser usado para contener la
				suma de los descuentos de línea o ítem.
			*/ 

			/* FACTOR DEL DESCUENTO -GLOBAL */
			var fac_dcto_glb = req.body.fac_dcto_glb;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* MONTO DE CARGO GLOBAL EN LA MONEDA DE LA TRANSACCION */
			var mnt_carg_glb = req.body.mnt_carg_glb;
			/*
				Distinto al elemento Total Cargos. Su propósito es permitir consignar en el comprobante de pago, un cargo a nivel global o
				total. Este campo no debe ser usado para contener la suma de los cargos de línea o ítem
			*/ 

			/* FACTOR DE CARGO GLOBAL */
			var fac_carg_glob = req.body.fac_carg_glb;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* TIPO DE CARGO GLOBAL */
			var tip_carg_glob = req.body.tip_carg_glob;
			/*
				Tipo Numerico logitud de 2, 50 – Otros cargos
											54 – Otros cargos relacionados al servicio
											55 – otros cargos no relacionados al servicio
			*/

			/* MONTO TOTAL PERCEPCION DE LA MONEDA DE LA TRANSACCION */
			var mnt_tot_per = req.body.mnt_tot_per;
			/*
				Tipo Numerico logitud de 12.2
			*/ 	

			/* CODIGO TIPO DE PERCEPCION */
			var tip_per = req.body.tip_per;
			/*
				Tipo Numerico logitud de 2, 51 – Percepción venta interna
											52 – Percepción a la adquisición de combustible
											53 - Percepción al agente de percepción con tasa especial
			*/  

			/* FACTOR DE PERCEPCION */
			var fac_per = req.body.fac_per;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* FACTOR DE PERCEPCION */
			var fac_per = req.body.fac_per;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* SUMATORIA TOTAL DE IMPUESTO EN MONEDA DE LA TRANSACCION */
			var mnt_tot_imp = req.body.mnt_tot_imp;
			/*
				Tipo Numerico logitud de 12.2, Corresponde a la suma de todos los impuestos ISC, IGV e IVAP
			*/ 

			/* SUMATORIA TOTAL DE VALORES DE VENTA */
			var mnt_tot_grv = req.body.mnt_tot_grv;
			/*
				Tipo Numerico logitud de 12.2
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES INAFECTAS */
			var mnt_tot_inf = req.body.mnt_tot_inf;
			/*
				Tipo Numerico logitud de 12.2, Contiene la informacion de la suma de todos los items por ventas inafectas
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES EXONERADAS */
			var mnt_tot_exr = req.body.mnt_tot_exr;
			/*
				Tipo Numerico logitud de 12.2, Contiene la informacion de la suma de todos los items por ventas inafectas
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES EXONERADAS */
			var mnt_tot_exe = req.body.mnt_tot_exe;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items exonerados
			*/ 

			/* SUMATORIA TOTAL GRATUITO */
			var mnt_tot_grt = req.body.mnt_tot_grt;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items gratuitos
			*/ 

			/* SUMATORIA TOTAL DEL VALOR DE VENTAS DE LAS OPERACIONES DE EXPORTACION */
			var mnt_tot_exp = req.body.mnt_tot_exp;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items exportaciones
			*/

			/* SUMATORIA TOTAL DE OPERACIONES AFECTAS A ISC */
			var mnt_tot_isc = req.body.mnt_tot_isc;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items ISC
			*/

			/* SUMATORIA TOTAL IGV */
			var mnt_tot_trb_igv = req.body.mnt_tot_trb_igv;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items IGV
			*/

			/* SUMATORIA TOTAL ISC */
			var mnt_tot_trb_isc = req.body.mnt_tot_trb_isc;
			/*
				Tipo Numerico logitud de 12.2, Corresponde al ISC total de la factura, la sumatorua no debe contener el ISC que corresponde a la transferencia de bienes
			*/

			/* SUMATORIA TOTAL DE OTROS TRIBUTOS EN SOLES */
			var mnt_tot_trb_otr = req.body.mnt_tot_trb_otr;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de otros tributos distinto al ISC y IGV
			*/

			/* MONTO TOTAL DE LA VENTA */
			var mnt_tot_val_vta = req.body.mnt_tot_val_vta;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items
			*/

			/* TOTAL PRECIO DE VENTA EN MONEDA DE LA TRANSACCION */
			var mnt_tot_prc_vta = req.body.mnt_tot_prc_vta;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total del valor de venta
			*/

			/* MONTO TOTAL DE DESCUENTO */
			var mnt_tot_dct = req.body.mnt_tot_dct;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los descuentos
			*/

			/* MONTO TOTAL DE OTROS CARGOS */
			var mnt_tot_otr_cgo = req.body.mnt_tot_otr_cgo;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de otros cargos
			*/

			/* MONTO TOTAL DE DESCUENTO */
			var mnt_tot_dcto = req.body.mnt_tot_dcto;
			/*

			/* IMPORTE TOTAL DE LA VENTA */
			var mnt_tot = parseFloat(req.body.mnt_tot).toFixed(2);
			/*
				Tipo Numerico logitud de 12.2, Suma de MON_TOT_PRC_VTA - MNT_TOT_DCT + MNT_TOT_OTR_CGO -
				menos los anticipos que hubieran sido recibidos
			*/

			/* MONTO TOTAL DE ANTICIPOS */
			var mnt_tot_antcp = req.body.mnt_tot_antcp;
			/*
				Tipo Numerico logitud de 12.2, Corresponde al total de anticipos del comprobante
			*/

			/* TIPO DE CAMBIO A CALCULAR A MONEDA NACIONAL */
			var tip_cmb = req.body.tip_cmb;
			/*
				Tipo Numerico logitud de 2.6, Enviar vacio en caso que el valor de moneda enviado sea soles (PEN)
			*/

			/* MONTO DESCUENTO GLOBAL DE MONEDA NACIONAL */
			var mnt_dcto_glb_nac = req.body.mnt_dcto_glb_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO CARGO GLOBAL EN MONEDA NACIONAL */
			var mnt_carg_glb_nac = req.body.mnt_carg_glb_nac;
			/*
				Tipo Numerico logitud de 12.2, Su proposito es consignar en el comprobante
			*/

			/* MONTO TOTAL DE PERCEPCION */
			var mnt_tot_per_nac = req.body.mnt_tot_per_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL IMPUESTOS NACIONAL */
			var mnt_tot_imp_nac = req.body.mnt_tot_imp_nac;
			/*
				Tipo Numerico logitud de 12.2, Corresonde a la suma total de todos los impuestos
			*/

			/* MONTO TOTAL GRAVADO */
			var mnt_tot_grv_nac = req.body.mnt_tot_grv_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL INAFECTO */
			var mnt_tot_inf_nac = req.body.mnt_tot_inf_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL EXONERADO */
			var mnt_tot_exr_nac = req.body.mnt_tot_exr_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL GRATUITO */
			var mnt_tot_grt_nac = req.body.mnt_tot_grt_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL EXPORTACION */
			var mnt_tot_exp_nac = req.body.mnt_tot_exp_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL IGV */
			var mnt_tot_trb_igv_nac = req.body.mnt_tot_trb_igv_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL ISC */
			var mnt_tot_trb_isc_nac = req.body.mnt_tot_trb_isc_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL OTROS TRIBUTOS */
			var mnt_tot_trb_otr_nac = (req.body.hasOwnProperty("mnt_tot_trb_otr_nac") && req.body.mnt_tot_trb_otr_nac !== undefined && req.body.mnt_tot_trb_otr_nac !== "") ? req.body.mnt_tot_trb_otr_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL VALOR DE VENTA */
			//var mnt_tot_val_vta_nac = req.body.mnt_tot_val_vta_nac;
			var mnt_tot_val_vta_nac = (req.body.hasOwnProperty("mnt_tot_val_vta_nac") && req.body.mnt_tot_val_vta_nac !== undefined && req.body.mnt_tot_val_vta_nac !== "") ? req.body.mnt_tot_val_vta_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL PRECIO DE VENTA */
			//var mnt_tot_prc_vta_nac = req.body.mnt_tot_prc_vta_nac;
			var mnt_tot_prc_vta_nac = (req.body.hasOwnProperty("mnt_tot_prc_vta_nac") && req.body.mnt_tot_prc_vta_nac !== undefined && req.body.mnt_tot_prc_vta_nac !== "") ? req.body.mnt_tot_prc_vta_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL DESCUENTO */
			//var mnt_tot_dcto_nac = req.body.mnt_tot_dcto_nac;
			var mnt_tot_dcto_nac = (req.body.hasOwnProperty("mnt_tot_dcto_nac") && req.body.mnt_tot_dcto_nac !== undefined && req.body.mnt_tot_dcto_nac !== "") ? req.body.mnt_tot_dcto_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL OTROS CARGOS */
			var mnt_tot_cgo_nac = req.body.mnt_tot_cgo_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL */
			var mnt_tot_nac = req.body.mnt_tot_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL ANTICIPO */
			var mnt_tot_antcp_nac = req.body.mnt_tot_antcp_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* CODIGO DEL BIEN O PRODUCTO */
			var cod_tip_detraccion = req.body.cod_tip_detraccion;
			/*
				Tipo Numerico logitud de 3, Otros servicios empresariales       "022"
											Arrendamiento de bienes muebles     "019"
											Demás servicios gravados con el IGV "037"
			*/

			/* MONTO TOTAL DETRACCION */
			var mnt_tot_detraccion = req.body.mnt_tot_detraccion;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* PORCENTAJE DETRACCION */
			var fac_detraccion = req.body.fac_detraccion;
			/*
				Tipo Numerico logitud de 3.2
			*/

			/* CODIGO DEL SOFTWARE DE FACTURACCION */
			var cod_sof_fact = req.body.cod_sof_fact;
			/*
				Tipo Texto logitud de 20
			*/

			/* CODIGO DE TIPO NOTA DE CREDITO */
			var cod_tip_nc = req.body.cod_tip_nc;
			/*
				Tipo Texto logitud de 2 (Dejar en blanco si el CPE no es Nota de Crédito)
											01 – Anulación de la operación
											02 – Anulación por error en el RUC
											03 – Corrección por error en la descripción
											04 – Descuento global (solo para factura)
											05 – Descuento por ítem (solo para factura)
											06 – Devolución total
											07 – Devolución por ítem
											08 – Bonificación (solo para factura)
											09 – Disminución en el valor
											10 – Otros conceptos
			*/

			/* CODIGO DE TIPO NOTA DE DEBITO */
			var cod_tip_nd = req.body.cod_tip_nd;
			/*
				Tipo Texto logitud de 2 (Dejar en blanco si el CPE no es Nota de Débito)
											01 – Intereses por mora
											02 – Aumento de valor
											03 – Penalidades
			*/

			/* TEXTO DE DESCRIPCION DEL MOTIVO CPE */
			var des_mtvo_nc_nd = req.body.des_mtvo_nc_nd;
			/*
				Tipo Texto logitud de 250
			*/

			/* CORREO DE ENVIO */
			var correo_envio = req.body.correo_envio;
			/*
				Tipo Texto logitud de 300
			*/

			/* CORREO COPIA */
			var correo_copia = req.body.correo_copia;
			/*
				Tipo Texto logitud de 300
			*/

			/* CORREO OCULTO */
			var correo_oculto = req.body.correo_oculto;
			/*
				Tipo Texto logitud de 300
			*/

			/* IDENTIFICADOR DEL PUNTO DE VENTA */
			var pto_vta = req.body.pto_vta;
			/*
				Tipo Texto logitud de 10
			*/

			/* FLAG DE TIPO DE CAMBIO */
			var flg_tip_cambio = req.body.flg_tip_cambio;
			/*
				Siempre enviar el valor "1"
			*/

			/* CODIGO SISTEMAS */
			var cod_procedencia = req.body.cod_procedencia;
			/*
				Siempre enviar el valor "002"
			*/

			/* CODIGO SISTEMAS */
			var id_ext_rzn = req.body.id_ext_rzn;
			/*
				Siempre enviar el valor "1"
			*/

			/* CODIGO SISTEMAS */
			var etd_snt = req.body.etd_snt;
			/*
				Enviar etiqueta vacia
			*/

			/* DESCRIPCION REFERENCIA DEL CLIENTE */
			var des_ref_clt = req.body.des_ref_clt;
			/*
				Enviar Vacio
			*/
		/* FIN VARIABLES DE CONTENIDO Y ENCABEZADO */

		/* INICIO VARIABLES DETALLE DE FACTURAS */

			/* NUMERO DEL CONSECUTIVO DEL ITEMS */
			var lin_itm = req.body.lin_itm;
			/*
				Numerico longitud 3, Campo obligatorio corresponde al numero de linea secuencial
			*/

			/* UNIDAD DE MEDIDA POR ITEMS */
			var cod_und_itm = req.body.cod_und_itm;
			/*
				Para la agencia corresponde ZZ
											BJ – Balde / BG – Bolsa / BO – Botellas / BX – Caja / CT –
											Cartones / CY – Cilindro / CJ – Conos / GRM – Gramo / SET –
											Juego / KGM – Kilogramo / KTM – Kilometro / KT – Kit / CA –
											Latas / LBR – Libras / LTR – Litro / MTR – Metro / MLL – Millares
											/
											NIU - Unidad (Bienes)
											ZZ - Unidad (Servicios)
			*/

			/* CANTIDAD DE UNIDADES POR ITEMS */
			var cant_und_itm = req.body.cant_und_itm;
			/*
				Numerico longitud 12.2, Cantidad de productos vendidos
			*/

			/* Valor de venta por línea/ítem (No incluye IGV, ISC, Otros Tributos) */
			var val_vta_itm = req.body.val_vta_itm;
			/*
				Numerico longitud 12.2, Obligatorio. Este elemento es el producto de la cantidad por el
				valor unitario (Q x Valor Unitario) y la deducción de descuentos aplicados a dicho ítem (de existir).
				Este importe no incluye los tributos (IGV, ISC y otros Tributos), los descuentos globales o cargos.
			*/

			/* Precio unitario de Venta por línea/ítem. (Incluye IGV, ISC y Otros tributos) */
			var prc_vta_und_itm = req.body.prc_vta_und_itm;
			/*
				Obligatorio. Dentro del ámbito tributario, es el monto correspondiente al precio unitario facturado del bien vendido o
				servicio vendido. Este monto es la suma total que queda obligado a pagar el adquirente o usuario por cada bien o
				servicio. Esto incluye los tributos (IGV, ISC y otros Tributos) y la deducción de descuentos por ítem		
			*/

			/* Valor unitario por ítem */
			var val_unit_itm = req.body.val_unit_itm;
			/*
				Valor de venta unitario por ítem, solo de corresponder
			*/

			/* Monto de IGV por ítem */
			var mnt_igv_itm = req.body.mnt_igv_itm;
			/*
				Es obligatorio solo de corresponder	
			*/

			/* Porcentaje de IGV por ítem */
			var por_igv_itm = req.body.por_igv_itm;
			/*
				Es obligatorio solo de corresponder	
			*/

			/* Importe de Venta por línea/ítem. (Incluye IGV, ISC y Otros tributos) */
			var prc_vta_item = req.body.prc_vta_item;
			/*
			*/

			/* Valor de Venta por Ítem antes de Aplicar Descuentos y Cargos. */
			var val_vta_brt_item = req.body.val_vta_brt_item;
			/*
				Si no existe Descuentos y Cargos , el valor debe ser igual a "VAL_VTA_ITEM".
			*/

			/* Código de tipo de Afectación al IGV por ítem */
			var cod_tip_afect_igv_itm = req.body.cod_tip_afect_igv_itm;
			/*
							10 – Gravado – operación onerosa
							11 – gravado – retiro por premio
							12 – gravado -retiro por donación
							14- gravado – retiro
							15 – gravado – bonificaciones
							16 – gravado- retiro por entrega a trabajadores
							17 – Gravado – IVAP
							20 – Exonerado – operación onerosa
							21 – Exonerado – operación onerosa
							30 – Inafecto – operación onerosa
							31 – Inafecto – retiro por bonificación
							32 – Inafecto – retiro
							33 – Inafecto – Retiro por muestras médicas
							34 – Inafecto – retiro por convenio colectivo
							35 – inafecto – retiro por premio
							36 – Inafecto – retiro por publicidad
							40 – exportación
			*/

			/* MONTO DE ISC POR ITEM */
			var mnt_isc_itm = req.body.mnt_isc_itm;
			/*
				Es obligatorio solo de corresponder
			*/

			/* PORCENTAJE DE ISC POR ITEM */
			var por_isc_itm = req.body.por_isc_itm;
			/*
				Es obligatorio solo de corresponder
			*/

			/* CODIGO DEL TIPO DE ISC */
			var cod_tip_sist_isc = req.body.cod_tip_sist_isc;
			/*
				se consigna el código de tipo de Sistema de ISC Aplicado, puede tomar los siguientes valores:
							01- Sistema al valor
							02- Aplicación del Monto Fijo
							03- Sistema de Precios de Venta al Público
			*/

			/* PRECIO SUGERIDO PARA ISC */
			var precio_sugerido_isc = req.body.precio_sugerido_isc;
			/*
				En los casos que no se tenga este dato, se debe pasar el valor venta del producto/servicio. Este dato multiplicado por el
				porcentaje del ISC se obtiene el Monto del ISC MNT_ISC_ITM= POR_ISC_ITM* PRECIO_SUGERIDO_ISC.
			*/

			/* MONTO DE DESCUENTO POR ITEM */
			var mnt_dcto_itm = req.body.mnt_dcto_itm;
			/*
			*/

			/* FACTOR DEL DESCUENTO POR ITEM */
			var fac_dcto_itm = req.body.fac_dcto_itm;
			/*
			*/

			/* MONTO DEL CARGO POR ITEM */
			var mnt_carg_itm = req.body.mnt_carg_itm;
			/*
			*/

			/* Factor del Cargo por ítem */
			var fac_carg_itm = req.body.fac_carg_itm;
			/*
				Debe expresarse en número, no en porcentaje. Ejemplo: si el porcentaje es 2%, consignar 0.02
			*/

			/* Tipo de Cargo por ítem */
			var tip_carg_itm = req.body.tip_carg_itm;
			/*
							50 – Otros cargos
							54 – Otros cargos relacionados al servicio
							55 – otros cargos no relacionados al servicio
			*/

			/* Monto Total Percepción por ítem en la Moneda de la Transacción */
			var mnt_tot_per_itm = req.body.mnt_tot_per_itm;
			/*
			*/

			/* Factor de la Percepción por ítem */
			var fac_per_itm = req.body.fac_per_itm;
			/*
				Debe expresarse en número, no en porcentaje. Ejemplo: si el porcentaje es 2%, consignar 0.02
			*/

			/* Código de Tipo de Percepción por ítem */
			var tip_per_itm = req.body.tip_per_itm;
			/*
								51 – Percepción venta interna
								52 – Percepción a la adquisición de combustible
								53 - Percepción al agente de percepción con tasa especial
			*/

			/* Descripción detallada del servicio prestado, bien vendido o cedido en uso, indicando el nombre y las características, tales como marca del bien vendido o cedido en uso. */
			var txt_des_itm = req.body.txt_des_itm;
			/*
				Obligatorio
			*/

			/* Glosa descriptiva Detalle Adicional por Ítem (Opcional) */
			var txt_des_adic_itm = req.body.txt_des_adic_itm;
			/*
			*/

			/* Código de producto del ítem */
			var cod_itm = req.body.cod_itm;
			/*
				Opcional. Su uso será obligatorio si el emisor electrónico, opta por consignar este código, en reemplazo de la descripción detallada
			*/

			/* Código de producto SUNAT */
			var cod_itm_sunat = req.body.cod_itm_sunat;
			/*
				Opcional. Código del producto de acuerdo al estándar internacional de la ONU denominado: United Nations Standard
				Products and Services Code - Código de productos y servicios estándar de las Naciones Unidas
			*/

			/* Número de documento del huésped */
			var hpj_num_doc = req.body.hpj_num_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del tipo de documento del huésped */
			var hpj_tip_doc = req.body.hpj_tip_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del país de emisión del pasaporte */
			var hpj_pais_emi_pasp = req.body.hpj_pais_emi_pasp;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Nombres y apellidos o denominación social del huésped */
			var hpj_nom_ape_rzn_scl = req.body.hpj_nom_ape_rzn_scl;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del país de residencia del huésped */
			var hpj_pais_res = req.body.hpj_pais_res;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Número de días de permanencia */
			var hpj_num_dias_per = req.body.hpj_num_dias_per;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de ingreso al establecimiento */
			var hpj_fec_ing = req.body.hpj_fec_ing;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de salida al establecimiento */
			var hpj_fec_sal = req.body.hpj_fec_sal;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de consumo del huesped */
			var hpj_fec_csm = req.body.hpj_fec_csm;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Documento de identidad del huésped */
			var pqt_num_doc = req.body.pqt_num_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Codigo de tipo de documento del huésped */
			var pqt_tip_doc = req.body.pqt_tip_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Nombre del huésped */
			var pqt_nom_doc = req.body.pqt_nom_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Numero de Asientos */
			var tpt_num_asnt = req.body.tpt_num_asnt;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Valor del detalle adicional 01 */
			var det_val_adic01 = req.body.det_val_adic01;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 02 */
			var det_val_adic02 = req.body.det_val_adic02;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 03 */
			var det_val_adic03 = req.body.det_val_adic03;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 04 */
			var det_val_adic04 = req.body.det_val_adic04;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 05 */
			var det_val_adic05 = req.body.det_val_adic05;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 06 */
			var det_val_adic06 = req.body.det_val_adic06;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 07 */
			var det_val_adic07 = req.body.det_val_adic07;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 08 */
			var det_val_adic08 = req.body.det_val_adic08;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 09 */
			var det_val_adic09 = req.body.det_val_adic09;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 10 */
			var det_val_adic10 = req.body.det_val_adic10;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Descripcion del ITEM Complementaria */
			var des_comp = req.body.des_comp;
			/*	
			*/

			/*	FIN DE CARGA DE DATOS DETALLES	*/
			/*  INICIO VARIABLES DOCUMENTOS REFERENCIAS	*/

			/* Numero secuencial del Documento de Referencia */
			var num_lin_ref = req.body.num_lin_ref;
			/*	
				Solo de corresponder
			*/

			/* Código de Tipo de Documento de referencia */
			var des_comp = req.body.des_comp;
			/*	
							01 – Factura
							02 – Boleta de venta
							07 – Nota de crédito
							08 – Nota de débito
			*/

			/* Serie de Documento de Referencia. */
			var num_serie_cpe_ref = req.body.num_serie_cpe_ref;
			/*	
				Es obligatorio solo de corresponder
			*/

			/* Numero correlativo de documento referencia */
			var num_corre_cpe_ref = req.body.num_corre_cpe_ref;
			/*	
				Es obligatorio solo de corresponder
			*/

			/* Fecha de emision del documento de referencia */
			var fec_doc_ref = req.body.fec_doc_ref;
			/*	
				El formato debe ser AAAA-MM-DD
			*/

			/* Código de Tipo de Otro Documento de Referencia. */
			var cod_tip_otr_doc_ref = req.body.cod_tip_otr_doc_ref;
			/*	
								Solo de corresponder
								01 – Factura emitida para corregir error en el RUC
								02 – Factura emitida por anticipos
								03 – Boleta de venta emitida por anticipos
								04 – Ticket de salida ENAPU
								05 – código SCOP
								99 - Otros

								01 => FACTURA
								03 => BOLETA VENTA
								07 => NOTA CRED
								08 => NOTA DEB
			*/

			/* Número de Otro Documento de Referencia. */
			var num_otr_doc_ref = req.body.num_otr_doc_ref;
			/*	
				Solo de corresponder
			*/

			/*		vValores de Notas de Credito y Debito
			*/
            var des_mtvo_nc_nd = req.body.des_mtvo_nc_nd;
            var cod_tip_doc_ref = req.body.cod_tip_doc_ref;
            var num_serie_cpe_ref = req.body.num_serie_cpe_ref;
        	var num_corre_cpe_ref = req.body.num_corre_cpe_ref;
        	var fec_doc_ref = req.body.fec_doc_ref;
        	var cod_tip_nc = req.body.cod_tip_nc;
        	var cod_tip_nd = req.body.cod_tip_nd;
        	var tip_cpe = req.body.tip_cpe;
        	var serie_corre_cpe_ref = req.body.serie_corre_cpe_ref;


			/* CAMPO DE ENVIO DE EMAIL */
			var email = req.body.email;
			
			var args = {
			"oUser": oUser,//util.taxtech.user
			"oPass": oPass,//util.taxtech.pass
			"oCabecera": {
					"ID": "876178c8fc875c949a0b7df5f716985d8338fa33e5a4017ebffb623ba8451d02",
					"COD_GPO": "1",
					"TIP_CPE": tip_cpe,
					"FEC_EMI": fec_emi,
					"HOR_EMI": hor_emi,
					"SERIE": serie,
					"CORRELATIVO": correlativo,
					"MONEDA": moneda,
					"COD_TIP_OPE": cod_tip_ope,
					"TIP_DOC_EMI": "6",
					"NRO_DOC_EMI": nro_doc_emi,//util.taxtech.ruc
					"NOM_EMI": nom_emi,
					"NOM_COM_EMI": nom_com_emi,
					"COD_LOC_EMI": "0000",
					"TIP_DOC_RCT": tip_doc_rct,
					"NRO_DOC_RCT": nro_doc_rct,
					"NOM_RCT": nom_rct,
					"DIR_DES_RCT": dir_des_rct,
					"UBI_RCT": "150101",
					"TIP_PAG": tip_pag,
					"FRM_PAG": frm_pag,
					"NRO_ORD_COM": "OC-235466",
					"FMT_IMPR": "001",
					"MNT_DCTO_GLB": mnt_dcto_glb,
					"FAC_DCTO_GLB": fac_dcto_glb,
					"MNT_CARG_GLB": mnt_carg_glb,
					"FAC_CARG_GLOB": fac_carg_glob,
					"MNT_TOT_PER": mnt_tot_per,
					"FAC_PER": fac_per,
					"MNT_TOT_IMP": mnt_tot_imp,
					"MNT_TOT_GRV": mnt_tot_grv,
					"MNT_TOT_INF": mnt_tot_inf,
					"MNT_TOT_EXR": mnt_tot_exr,
					"MNT_TOT_GRT": mnt_tot_grt,
					"MNT_TOT_EXP": mnt_tot_exp,
					"MNT_TOT_TRB_IGV": mnt_tot_imp,
					"MNT_TOT_TRB_ISC": mnt_tot_trb_isc,
					"MNT_TOT_TRB_OTR": mnt_tot_trb_otr,
					"MNT_TOT_VAL_VTA": mnt_tot_val_vta,
					"MNT_TOT_PRC_VTA": mnt_tot_prc_vta,
					"MNT_TOT_DCTO": mnt_tot_dcto,
					"MNT_TOT_OTR_CGO": mnt_tot_otr_cgo,
					"MNT_TOT": mnt_tot,
					"MNT_TOT_ANTCP": mnt_tot_antcp,
					"MNT_DCTO_GLB_NAC": mnt_dcto_glb_nac,
					"MNT_CARG_GLB_NAC": mnt_carg_glb_nac,
					"MNT_TOT_PER_NAC": mnt_tot_per_nac,
					"MNT_TOT_IMP_NAC": mnt_tot_imp_nac,
					"MNT_TOT_GRV_NAC": mnt_tot_grv_nac,
					"MNT_TOT_INF_NAC": mnt_tot_inf_nac,
					"MNT_TOT_EXR_NAC": mnt_tot_exr_nac,
					"MNT_TOT_GRT_NAC": mnt_tot_grt_nac,
					"MNT_TOT_EXP_NAC": mnt_tot_exp_nac,
					"MNT_TOT_TRB_IGV_NAC": mnt_tot_trb_igv_nac,
					"MNT_TOT_TRB_ISC_NAC": mnt_tot_trb_isc_nac,
					"MNT_TOT_TRB_OTR_NAC": mnt_tot_trb_otr_nac,
					"MNT_TOT_VAL_VTA_NAC": mnt_tot_val_vta_nac,
					"MNT_TOT_PRC_VTA_NAC": mnt_tot_prc_vta_nac,
					"MNT_TOT_DCTO_NAC": mnt_tot_dcto_nac,
					"MNT_TOT_OTR_CGO_NAC": "0.00",
					"MNT_TOT_NAC": mnt_tot_nac,
					"MNT_TOT_ANTCP_NAC": mnt_tot_antcp_nac,
					"COD_TIP_DETRACCION": cod_tip_detraccion,
					"MNT_TOT_DETRACCION": mnt_tot_detraccion,
					"FAC_DETRACCION": fac_detraccion,
					"COD_SOF_FACT": "COD001",
	        		"COD_TIP_NC": cod_tip_nc,
	        		"COD_TIP_ND": cod_tip_nd,
				    "DES_MTVO_NC_ND": des_mtvo_nc_nd,
					"CORREO_ENVIO": email,
					"COD_PROCEDENCIA": "003",
					"ID_EXT_RZN": "1",
					"ETD_SNT": "101"

				},

				"listDetalle": {},

				"listAdicionales": {
					"CPE_DAT_ADIC_BE": [
						{
							"COD_TIP_ADIC_SUNAT": "38",
							"NUM_LIN_ADIC_SUNAT": "38",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "39",
							"NUM_LIN_ADIC_SUNAT": "39",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "40",
							"NUM_LIN_ADIC_SUNAT": "40",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "50",
							"NUM_LIN_ADIC_SUNAT": "50",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "60",
							"NUM_LIN_ADIC_SUNAT": "60",
							"TXT_DESC_ADIC_SUNAT": ""
						}
					]
				},

				"listReferenciados": {
					"CPE_DOC_REF_BE": 
						{
	                    "COD_TIP_DOC_REF": cod_tip_doc_ref,
	        			"COD_TIP_OTR_DOC_REF": cod_tip_otr_doc_ref,
	        			"FEC_DOC_REF": fec_doc_ref,
	        			"NUM_CORRE_CPE_REF": num_corre_cpe_ref,
						"NUM_LIN_REF": "001",
						"NUM_OTR_DOC_REF": num_otr_doc_ref,
						"NUM_SERIE_CPE_REF": num_serie_cpe_ref,
	        			"SERIE_CORRE_CPE_REF": serie_corre_cpe_ref,
	        			"NUM_OTR_DOC_REF": num_otr_doc_ref
						}
				},

			"listAnticipos": null,
			"listFacGuia": null,
			"listRelacionado": null,
			"oTipoOnline": "Normal"
			};

			/* detalles de factura */
			var list_data = {};
			var list_service_doc_arr = [];
			if(req.body.hasOwnProperty("list_service_doc")){
				if(req.body.list_service_doc !== ""){
					var list_service_doc = JSON.parse(req.body.list_service_doc);
					console.log(list_service_doc);
					for(var i = 0;i < list_service_doc.length;i++){
						//OBTENIENDO VARIABLES
						var current_deta = list_service_doc[i];
						var cod_itm = (current_deta.cod_itm !== undefined && current_deta.cod_itm !== "") ? parseFloat(current_deta.cod_itm) : 0.00;
						var cod_und_itm = (current_deta.cod_und_itm !== undefined && current_deta.cod_und_itm !== "") ? current_deta.cod_und_itm : "";
						var txt_des_itm = (current_deta.txt_des_itm !== undefined && current_deta.txt_des_itm !== "") ? current_deta.txt_des_itm : "";
						var txt_des_adic_itm = (current_deta.txt_des_adic_itm !== undefined && current_deta.txt_des_adic_itm !== "") ? current_deta.txt_des_adic_itm : "";
						var cant_und_itm = (current_deta.cant_und_itm !== undefined && current_deta.cant_und_itm !== "") ? parseInt(current_deta.cant_und_itm) : 0;
						var val_vta_itm = (current_deta.val_vta_itm !== undefined && current_deta.val_vta_itm !== "") ? parseFloat(current_deta.val_vta_itm) : 0.00;
						var prc_vta_und_itm = (current_deta.prc_vta_und_itm !== undefined && current_deta.prc_vta_und_itm !== "") ? parseFloat(current_deta.prc_vta_und_itm) : 0.00;
						var prc_vta_item = (current_deta.prc_vta_item !== undefined && current_deta.prc_vta_item !== "") ? parseFloat(current_deta.prc_vta_item) : 0.00;
						var cod_tip_afect_igv_itm = (current_deta.cod_tip_afect_igv_itm !== undefined && current_deta.cod_tip_afect_igv_itm !== "") ? parseFloat(current_deta.cod_tip_afect_igv_itm) : 0.00;
						var por_igv_itm = (current_deta.por_igv_itm !== undefined && current_deta.por_igv_itm !== "") ? parseFloat(current_deta.por_igv_itm) : 0.00;
						var val_vta_brt_item =  (current_deta.val_vta_brt_item !== undefined && current_deta.val_vta_brt_item !== "") ? parseFloat(current_deta.val_vta_brt_item) : 0.00;
						//var val_vta_itm_igv = val_vta_itm;
						var tributo = parseFloat(current_deta.service_doc_trib).toFixed(2);

						//===============================================
						var val_unit_itm = (current_deta.val_unit_itm !== undefined && current_deta.val_unit_itm !== "") ? parseFloat(current_deta.val_unit_itm) : 0.00;
						var mnt_igv_itm = (current_deta.mnt_igv_itm !== undefined && current_deta.mnt_igv_itm !== "") ? parseFloat(current_deta.mnt_igv_itm) : 0.00;

						//GENERANDO OBJETO
						var obj_service_doc = {};
						obj_service_doc.LIN_ITM = (i + 1);
						obj_service_doc.COD_UND_ITM = cod_und_itm;
						obj_service_doc.CANT_UND_ITM = cant_und_itm;
						obj_service_doc.VAL_VTA_ITM = val_vta_itm; //val_vta_itm * cant_und_itm
						obj_service_doc.PRC_VTA_UND_ITM = parseFloat(prc_vta_und_itm).toFixed(2);
						obj_service_doc.VAL_UNIT_ITM = parseFloat(val_unit_itm).toFixed(2);
						obj_service_doc.MNT_IGV_ITM = mnt_igv_itm;
						obj_service_doc.POR_IGV_ITM = por_igv_itm;
						//obj_service_doc.PRC_VTA_ITEM = parseFloat(((((val_vta_itm)*tributo)-(val_vta_itm))*(-1))*(cant_und_itm)).toFixed(2);
						obj_service_doc.PRC_VTA_ITEM = prc_vta_item;
						obj_service_doc.VAL_VTA_BRT_ITEM = val_vta_brt_item;
						obj_service_doc.COD_TIP_AFECT_IGV_ITM = cod_tip_afect_igv_itm;
						obj_service_doc.TXT_DES_ITM = txt_des_itm;
						obj_service_doc.TXT_DES_ADIC_ITM = txt_des_adic_itm;
						obj_service_doc.COD_ITM = cod_itm;
						list_service_doc_arr.push(obj_service_doc);
					}
					list_data.CPE_DETALLE_BE = list_service_doc_arr;
					args.listDetalle = list_data;
				}
			}			
			logger.error("================================= // =============================================");
			logger.error(args);
			logger.error("================================= // =============================================");
			soap.createClient(url, function(err, client) {
			client.callProcessOnline(args, function(err, result) {
				if(err){
					return res.send(400, {'success':false,'data' : []});
				}else{
					res.send(200, {'success':true,'data' : result});
				}
			});
			});

		//}
	//});
});

router.post('/document/create', function (req, res, next) {

	var nro_doc = "";
	var query = document.getMaxNro(req.body);
	//util.connection.query(query, function (error, results){
		//if (!error) {
			var url = 'http://facturaenuna.pe/OnlineCPE/CPE/wsOnlineToCPE.svc?wsdl';

			//PARAMETROS FACTURA

			/* FECHA DE EMISION DEL COMPROBANTE */
			var fec_emi = req.body.fec_emi; 
			/*
				2018-07-27
			*/
			
			/* HORA DE EMISION DEL COMPROBANTE */
			var hor_emi = req.body.hor_emi;
			/* 
				08:56:14
			*/

			/* NUMERO DE SERIE DEL COMPROBANTE */
			var serie = req.body.serie;
			/* 
				F004
			*/

			/* NUMERO DEL CORRELATIVO DEL DOCUMENTO */
			/*
			var correlativo = 0;
			if(results[0].nro !== undefined){
				correlativo = parseInt(results[0].nro) + 1; 
			}else{
				correlativo = req.body.correlativo; 
			}

			console.log("===================");
			console.log(results[0].nro);
			console.log("===================");
			
			*/

			var correlativo = req.body.correlativo; 
			/* 
				00000020
			*/

			/* TIPO DE MONEDA */
			var moneda = req.body.moneda;
			/* 
				PEN
			*/

			/* CODIGO DE TIPO DE OPERACION */
			var cod_tip_ope = req.body.cod_tip_ope;
			/*
				0101 - Venta Interna / 
				0102 - Venta Interna - Anticipos / 
				0103 - Venta Interna - Deducción de Anticipos / 
				0104 - Venta Itinerante
			*/

			/* Código de Tipo de Identificador Fiscal del Emisor */
			var tip_doc_emi = req.body.tip_doc_emi;
			/* 
				6
			*/
			/* Número de identificador fiscal del emisor (RUC) */
			var nro_doc = req.body.nro_doc;
			/*
				20601890659
			*/

			/* Razón Social del Emisor */
			var nom_emi = req.body.nom_emi;
			/*
				TAX TECHNOLOGY PRUEBA SAC
			*/

			/* Nombre Comercial del Emisor  */
			var nom_com_emi = req.body.nom_com_emi;
			/*
				TAXTECH SAC
			*/

			/* Código de Tipo de Número de Identificador de Receptor (CLIENTE) */
			var tip_doc_rct = req.body.tip_doc_rct;
			/*
				0 – Doc. Trib. No Dom. Sin RUC.
				1 – DNI
				4 – Carnet de extranjería
				6 – RUC
				7 – Pasaporte
				A – Cedula diplomática de identidad
				B – Doc. Identi. País residencia No Domiciliado Tratándose de
				operaciones de exportación (COD_TIP_OPE = 0102) el código a
				utilizar será “-“
			*/

			/* NRO DOC DEL CLIENTE // Tratándose de operaciones de exportación (COD_TIP_OPE = 0102)  */
			var nro_doc_rct = req.body.nro_doc_rct;
			/*
				20601890000
			*/

			/* NOM CLIENTE RECEPTOR */
			var nom_rct = req.body.nom_rct;
			/*
				CLIENTE RECEPTOR S.A.C.
			*/

			/* Razón Social del Receptor (CLIENTE) */
			var dir_des_rct = req.body.dir_des_rct;
			/*
				AV. COMANDANTE ESPINAR NRO. 435 DPTO. 803 CJRES PEDRO RUIZ GALLO
			*/

			/* CODIGO DE UBIGEO DE LA PROVINCIA | DEPARTAMENTO | DISTRITO */
			var ubi_rct = req.body.ubi_rct;
			/*
				150101
			*/

			/* CODIGO DEL TIPO DE PAGO */
			var tip_pag = req.body.tip_pag;
			/*
				000 => NO ASIGNADO
				001 => CONTADO
				002 => CRÉDITO A 7 DÍAS
				003 => CRÉDITO A 15 DÍAS
				004 => CRÉDITO A 30 DÍAS
				005 => CRÉDITO A 60 DÍAS
				006 => CRÉDITO A 90 DÍAS
				007 => CRÉDITO A 120 DÍAS
				008 => CRÉDITO A 20 DÍAS
				009 => CRÉDITO A 45 DÍAS
			*/

			/* CODIGO DE FORMA DE PAGO */
			var frm_pag = req.body.frm_pag;
			/*
				000 NO ASIGNADO
				001 EFECTIVO
				002 CHEQUE
				003 LETRA
				004 TARJETA DE CRÉDITO
				005 TARJETA DE DÉBITO
				006 DEPOSITO BANCARIO
				007 TRANSFERENCIA INTERBANCARIA
			*/

			/* NUMERO DE ORDEN DE COMPRA */
			var nro_ord_com = req.body.nro_ord_com;
			/*
				OC-235466
			*/

			/* Código del Tipo de Guía Texto */
			var cod_tip_gre = req.body.cod_tip_gre;
			/*
				Tipo Texto logitud de 2
			*/ 

			/* Numero del Tipo de Guía Texto */
			var nro_gre = req.body.nro_gre;
			/*
				Tipo Texto logitud de 15
			*/ 

			/* Enviar etiqueta vacia */
			var cod_opc = req.body.cod_opc;
			/*
				Tipo Texto logitud de 50
			*/ 

			/* Forma de impresion */
			var fmt_impr = req.body.fmt;
			/*
				Tipo Texto logitud de 3, siempre enviar el valor "001"
			*/ 

			/* Impresora destino */
			var impresora = req.body.impresora;
			/*
				Tipo Texto logitud de 120, Enviar “nombre o IP del servidor”/”nombre de la impresora”
				como está configurada en la RED, siempre que la instalación sea in House. Caso contrario se envía Vacío.
			*/ 

			/* Monto descuento global en moneda de la transaccion */
			var mnt_dcto_glb = req.body.mnt_dcto_glb;
			/*
				Tipo Numerico logitud de 12.2, Distinto al elemento Total Descuentos. Su propósito es permitir
				consignar en el comprobante de pago, un descuento a nivel global o total. Este campo no debe ser usado para contener la
				suma de los descuentos de línea o ítem.
			*/ 

			/* FACTOR DEL DESCUENTO -GLOBAL */
			var fac_dcto_glb = req.body.fac_dcto_glb;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* MONTO DE CARGO GLOBAL EN LA MONEDA DE LA TRANSACCION */
			var mnt_carg_glb = req.body.mnt_carg_glb;
			/*
				Distinto al elemento Total Cargos. Su propósito es permitir consignar en el comprobante de pago, un cargo a nivel global o
				total. Este campo no debe ser usado para contener la suma de los cargos de línea o ítem
			*/ 

			/* FACTOR DE CARGO GLOBAL */
			var fac_carg_glob = req.body.fac_carg_glb;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* TIPO DE CARGO GLOBAL */
			var tip_carg_glob = req.body.tip_carg_glob;
			/*
				Tipo Numerico logitud de 2, 50 – Otros cargos
											54 – Otros cargos relacionados al servicio
											55 – otros cargos no relacionados al servicio
			*/

			/* MONTO TOTAL PERCEPCION DE LA MONEDA DE LA TRANSACCION */
			var mnt_tot_per = req.body.mnt_tot_per;
			/*
				Tipo Numerico logitud de 12.2
			*/ 	

			/* CODIGO TIPO DE PERCEPCION */
			var tip_per = req.body.tip_per;
			/*
				Tipo Numerico logitud de 2, 51 – Percepción venta interna
											52 – Percepción a la adquisición de combustible
											53 - Percepción al agente de percepción con tasa especial
			*/  

			/* FACTOR DE PERCEPCION */
			var fac_per = req.body.fac_per;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* FACTOR DE PERCEPCION */
			var fac_per = req.body.fac_per;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* SUMATORIA TOTAL DE IMPUESTO EN MONEDA DE LA TRANSACCION */
			var mnt_tot_imp = req.body.mnt_tot_imp;
			/*
				Tipo Numerico logitud de 12.2, Corresponde a la suma de todos los impuestos ISC, IGV e IVAP
			*/ 

			/* SUMATORIA TOTAL DE VALORES DE VENTA */
			var mnt_tot_grv = parseFloat(req.body.mnt_tot_grv).toFixed(2);
			/*
				Tipo Numerico logitud de 12.2
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES INAFECTAS */
			var mnt_tot_inf = req.body.mnt_tot_inf;
			/*
				Tipo Numerico logitud de 12.2, Contiene la informacion de la suma de todos los items por ventas inafectas
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES EXONERADAS */
			var mnt_tot_exr = req.body.mnt_tot_exr;
			/*
				Tipo Numerico logitud de 12.2, Contiene la informacion de la suma de todos los items por ventas inafectas
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES EXONERADAS */
			var mnt_tot_exe = req.body.mnt_tot_exe;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items exonerados
			*/ 

			/* SUMATORIA TOTAL GRATUITO */
			var mnt_tot_grt = req.body.mnt_tot_grt;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items gratuitos
			*/ 

			/* SUMATORIA TOTAL DEL VALOR DE VENTAS DE LAS OPERACIONES DE EXPORTACION */
			var mnt_tot_exp = req.body.mnt_tot_exp;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items exportaciones
			*/

			/* SUMATORIA TOTAL DE OPERACIONES AFECTAS A ISC */
			var mnt_tot_isc = req.body.mnt_tot_isc;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items ISC
			*/

			/* SUMATORIA TOTAL IGV */
			var mnt_tot_trb_igv = req.body.mnt_tot_trb_igv;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items IGV
			*/

			/* SUMATORIA TOTAL ISC */
			var mnt_tot_trb_isc = req.body.mnt_tot_trb_isc;
			/*
				Tipo Numerico logitud de 12.2, Corresponde al ISC total de la factura, la sumatorua no debe contener el ISC que corresponde a la transferencia de bienes
			*/

			/* SUMATORIA TOTAL DE OTROS TRIBUTOS EN SOLES */
			var mnt_tot_trb_otr = req.body.mnt_tot_trb_otr;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de otros tributos distinto al ISC y IGV
			*/

			/* MONTO TOTAL DE LA VENTA */
			var mnt_tot_val_vta = req.body.mnt_tot_val_vta;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items
			*/

			/* TOTAL PRECIO DE VENTA EN MONEDA DE LA TRANSACCION */
			var mnt_tot_prc_vta = req.body.mnt_tot_prc_vta;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total del valor de venta
			*/

			/* MONTO TOTAL DE DESCUENTO */
			var mnt_tot_dct = req.body.mnt_tot_dct;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los descuentos
			*/

			/* MONTO TOTAL DE OTROS CARGOS */
			var mnt_tot_otr_cgo = req.body.mnt_tot_otr_cgo;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de otros cargos
			*/

			/* MONTO TOTAL DE DESCUENTO */
			var mnt_tot_dcto = req.body.mnt_tot_dcto;
			/*

			/* IMPORTE TOTAL DE LA VENTA */
			var mnt_tot = parseFloat(req.body.mnt_tot).toFixed(2);
			/*
				Tipo Numerico logitud de 12.2, Suma de MON_TOT_PRC_VTA - MNT_TOT_DCT + MNT_TOT_OTR_CGO -
				menos los anticipos que hubieran sido recibidos
			*/

			/* MONTO TOTAL DE ANTICIPOS */
			var mnt_tot_antcp = req.body.mnt_tot_antcp;
			/*
				Tipo Numerico logitud de 12.2, Corresponde al total de anticipos del comprobante
			*/

			/* TIPO DE CAMBIO A CALCULAR A MONEDA NACIONAL */
			var tip_cmb = req.body.tip_cmb;
			/*
				Tipo Numerico logitud de 2.6, Enviar vacio en caso que el valor de moneda enviado sea soles (PEN)
			*/

			/* MONTO DESCUENTO GLOBAL DE MONEDA NACIONAL */
			var mnt_dcto_glb_nac = req.body.mnt_dcto_glb_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO CARGO GLOBAL EN MONEDA NACIONAL */
			var mnt_carg_glb_nac = req.body.mnt_carg_glb_nac;
			/*
				Tipo Numerico logitud de 12.2, Su proposito es consignar en el comprobante
			*/

			/* MONTO TOTAL DE PERCEPCION */
			var mnt_tot_per_nac = req.body.mnt_tot_per_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL IMPUESTOS NACIONAL */
			var mnt_tot_imp_nac = req.body.mnt_tot_imp_nac;
			/*
				Tipo Numerico logitud de 12.2, Corresonde a la suma total de todos los impuestos
			*/

			/* MONTO TOTAL GRAVADO */
			var mnt_tot_grv_nac = req.body.mnt_tot_grv_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL INAFECTO */
			var mnt_tot_inf_nac = req.body.mnt_tot_inf_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL EXONERADO */
			var mnt_tot_exr_nac = req.body.mnt_tot_exr_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL GRATUITO */
			var mnt_tot_grt_nac = req.body.mnt_tot_grt_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL EXPORTACION */
			var mnt_tot_exp_nac = req.body.mnt_tot_exp_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL IGV */
			var mnt_tot_trb_igv_nac = req.body.mnt_tot_trb_igv_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL ISC */
			var mnt_tot_trb_isc_nac = req.body.mnt_tot_trb_isc_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL OTROS TRIBUTOS */
			var mnt_tot_trb_otr_nac = req.body.mnt_tot_trb_otr_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL VALOR DE VENTA */
			var mnt_tot_val_vta_nac = req.body.mnt_tot_val_vta_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL PRECIO DE VENTA */
			var mnt_tot_prc_vta_nac = req.body.mnt_tot_prc_vta_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL DESCUENTO */
			var mnt_tot_dcto_nac = req.body.mnt_tot_dcto_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL OTROS CARGOS */
			var mnt_tot_cgo_nac = req.body.mnt_tot_cgo_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL */
			var mnt_tot_nac = req.body.mnt_tot_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL ANTICIPO */
			var mnt_tot_antcp_nac = req.body.mnt_tot_antcp_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* CODIGO DEL BIEN O PRODUCTO */
			var cod_tip_detraccion = req.body.cod_tip_detraccion;
			/*
				Tipo Numerico logitud de 3, Otros servicios empresariales       "022"
											Arrendamiento de bienes muebles     "019"
											Demás servicios gravados con el IGV "037"
			*/

			/* MONTO TOTAL DETRACCION */
			var mnt_tot_detraccion = req.body.mnt_tot_detraccion;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* PORCENTAJE DETRACCION */
			var fac_detraccion = req.body.fac_detraccion;
			/*
				Tipo Numerico logitud de 3.2
			*/

			/* CODIGO DEL SOFTWARE DE FACTURACCION */
			var cod_sof_fact = req.body.cod_sof_fact;
			/*
				Tipo Texto logitud de 20
			*/

			/* CODIGO DE TIPO NOTA DE CREDITO */
			var cod_tip_nc = req.body.cod_tip_nc;
			/*
				Tipo Texto logitud de 2 (Dejar en blanco si el CPE no es Nota de Crédito)
											01 – Anulación de la operación
											02 – Anulación por error en el RUC
											03 – Corrección por error en la descripción
											04 – Descuento global (solo para factura)
											05 – Descuento por ítem (solo para factura)
											06 – Devolución total
											07 – Devolución por ítem
											08 – Bonificación (solo para factura)
											09 – Disminución en el valor
											10 – Otros conceptos
			*/

			/* CODIGO DE TIPO NOTA DE DEBITO */
			var cod_tip_nd = req.body.cod_tip_nd;
			/*
				Tipo Texto logitud de 2 (Dejar en blanco si el CPE no es Nota de Débito)
											01 – Intereses por mora
											02 – Aumento de valor
											03 – Penalidades
			*/

			/* TEXTO DE DESCRIPCION DEL MOTIVO CPE */
			var des_mtvo_nc_nd = req.body.des_mtvo_nc_nd;
			/*
				Tipo Texto logitud de 250
			*/

			/* CORREO DE ENVIO */
			var correo_envio = req.body.correo_envio;
			/*
				Tipo Texto logitud de 300
			*/

			/* CORREO COPIA */
			var correo_copia = req.body.correo_copia;
			/*
				Tipo Texto logitud de 300
			*/

			/* CORREO OCULTO */
			var correo_oculto = req.body.correo_oculto;
			/*
				Tipo Texto logitud de 300
			*/

			/* IDENTIFICADOR DEL PUNTO DE VENTA */
			var pto_vta = req.body.pto_vta;
			/*
				Tipo Texto logitud de 10
			*/

			/* FLAG DE TIPO DE CAMBIO */
			var flg_tip_cambio = req.body.flg_tip_cambio;
			/*
				Siempre enviar el valor "1"
			*/

			/* CODIGO SISTEMAS */
			var cod_procedencia = req.body.cod_procedencia;
			/*
				Siempre enviar el valor "002"
			*/

			/* CODIGO SISTEMAS */
			var id_ext_rzn = req.body.id_ext_rzn;
			/*
				Siempre enviar el valor "1"
			*/

			/* CODIGO SISTEMAS */
			var etd_snt = req.body.etd_snt;
			/*
				Enviar etiqueta vacia
			*/

			/* DESCRIPCION REFERENCIA DEL CLIENTE */
			var des_ref_clt = req.body.des_ref_clt;
			/*
				Enviar Vacio
			*/
		/* FIN VARIABLES DE CONTENIDO Y ENCABEZADO */

		/* INICIO VARIABLES DETALLE DE FACTURAS */

			/* NUMERO DEL CONSECUTIVO DEL ITEMS */
			var lin_itm = req.body.lin_itm;
			/*
				Numerico longitud 3, Campo obligatorio corresponde al numero de linea secuencial
			*/

			/* UNIDAD DE MEDIDA POR ITEMS */
			var cod_und_itm = req.body.cod_und_itm;
			/*
				Para la agencia corresponde ZZ
											BJ – Balde / BG – Bolsa / BO – Botellas / BX – Caja / CT –
											Cartones / CY – Cilindro / CJ – Conos / GRM – Gramo / SET –
											Juego / KGM – Kilogramo / KTM – Kilometro / KT – Kit / CA –
											Latas / LBR – Libras / LTR – Litro / MTR – Metro / MLL – Millares
											/
											NIU - Unidad (Bienes)
											ZZ - Unidad (Servicios)
			*/

			/* CANTIDAD DE UNIDADES POR ITEMS */
			var cant_und_itm = req.body.cant_und_itm;
			/*
				Numerico longitud 12.2, Cantidad de productos vendidos
			*/

			/* Valor de venta por línea/ítem (No incluye IGV, ISC, Otros Tributos) */
			var val_vta_itm = req.body.val_vta_itm;
			/*
				Numerico longitud 12.2, Obligatorio. Este elemento es el producto de la cantidad por el
				valor unitario (Q x Valor Unitario) y la deducción de descuentos aplicados a dicho ítem (de existir).
				Este importe no incluye los tributos (IGV, ISC y otros Tributos), los descuentos globales o cargos.
			*/

			/* Precio unitario de Venta por línea/ítem. (Incluye IGV, ISC y Otros tributos) */
			var prc_vta_und_itm = req.body.prc_vta_und_itm;
			/*
				Obligatorio. Dentro del ámbito tributario, es el monto correspondiente al precio unitario facturado del bien vendido o
				servicio vendido. Este monto es la suma total que queda obligado a pagar el adquirente o usuario por cada bien o
				servicio. Esto incluye los tributos (IGV, ISC y otros Tributos) y la deducción de descuentos por ítem		
			*/

			/* Valor unitario por ítem */
			var val_unit_itm = req.body.val_unit_itm;
			/*
				Valor de venta unitario por ítem, solo de corresponder
			*/

			/* Monto de IGV por ítem */
			var mnt_igv_itm = req.body.mnt_igv_itm;
			/*
				Es obligatorio solo de corresponder	
			*/

			/* Porcentaje de IGV por ítem */
			var por_igv_itm = req.body.por_igv_itm;
			/*
				Es obligatorio solo de corresponder	
			*/

			/* Importe de Venta por línea/ítem. (Incluye IGV, ISC y Otros tributos) */
			var prc_vta_item = req.body.prc_vta_item;
			/*
			*/

			/* Valor de Venta por Ítem antes de Aplicar Descuentos y Cargos. */
			var val_vta_brt_item = req.body.val_vta_brt_item;
			/*
				Si no existe Descuentos y Cargos , el valor debe ser igual a "VAL_VTA_ITEM".
			*/

			/* Código de tipo de Afectación al IGV por ítem */
			var cod_tip_afect_igv_itm = req.body.cod_tip_afect_igv_itm;
			/*
							10 – Gravado – operación onerosa
							11 – gravado – retiro por premio
							12 – gravado -retiro por donación
							14- gravado – retiro
							15 – gravado – bonificaciones
							16 – gravado- retiro por entrega a trabajadores
							17 – Gravado – IVAP
							20 – Exonerado – operación onerosa
							21 – Exonerado – operación onerosa
							30 – Inafecto – operación onerosa
							31 – Inafecto – retiro por bonificación
							32 – Inafecto – retiro
							33 – Inafecto – Retiro por muestras médicas
							34 – Inafecto – retiro por convenio colectivo
							35 – inafecto – retiro por premio
							36 – Inafecto – retiro por publicidad
							40 – exportación
			*/

			/* MONTO DE ISC POR ITEM */
			var mnt_isc_itm = req.body.mnt_isc_itm;
			/*
				Es obligatorio solo de corresponder
			*/

			/* PORCENTAJE DE ISC POR ITEM */
			var por_isc_itm = req.body.por_isc_itm;
			/*
				Es obligatorio solo de corresponder
			*/

			/* CODIGO DEL TIPO DE ISC */
			var cod_tip_sist_isc = req.body.cod_tip_sist_isc;
			/*
				se consigna el código de tipo de Sistema de ISC Aplicado, puede tomar los siguientes valores:
							01- Sistema al valor
							02- Aplicación del Monto Fijo
							03- Sistema de Precios de Venta al Público
			*/

			/* PRECIO SUGERIDO PARA ISC */
			var precio_sugerido_isc = req.body.precio_sugerido_isc;
			/*
				En los casos que no se tenga este dato, se debe pasar el valor venta del producto/servicio. Este dato multiplicado por el
				porcentaje del ISC se obtiene el Monto del ISC MNT_ISC_ITM= POR_ISC_ITM* PRECIO_SUGERIDO_ISC.
			*/

			/* MONTO DE DESCUENTO POR ITEM */
			var mnt_dcto_itm = req.body.mnt_dcto_itm;
			/*
			*/

			/* FACTOR DEL DESCUENTO POR ITEM */
			var fac_dcto_itm = req.body.fac_dcto_itm;
			/*
			*/

			/* MONTO DEL CARGO POR ITEM */
			var mnt_carg_itm = req.body.mnt_carg_itm;
			/*
			*/

			/* Factor del Cargo por ítem */
			var fac_carg_itm = req.body.fac_carg_itm;
			/*
				Debe expresarse en número, no en porcentaje. Ejemplo: si el porcentaje es 2%, consignar 0.02
			*/

			/* Tipo de Cargo por ítem */
			var tip_carg_itm = req.body.tip_carg_itm;
			/*
							50 – Otros cargos
							54 – Otros cargos relacionados al servicio
							55 – otros cargos no relacionados al servicio
			*/

			/* Monto Total Percepción por ítem en la Moneda de la Transacción */
			var mnt_tot_per_itm = req.body.mnt_tot_per_itm;
			/*
			*/

			/* Factor de la Percepción por ítem */
			var fac_per_itm = req.body.fac_per_itm;
			/*
				Debe expresarse en número, no en porcentaje. Ejemplo: si el porcentaje es 2%, consignar 0.02
			*/

			/* Código de Tipo de Percepción por ítem */
			var tip_per_itm = req.body.tip_per_itm;
			/*
								51 – Percepción venta interna
								52 – Percepción a la adquisición de combustible
								53 - Percepción al agente de percepción con tasa especial
			*/

			/* Descripción detallada del servicio prestado, bien vendido o cedido en uso, indicando el nombre y las características, tales como marca del bien vendido o cedido en uso. */
			var txt_des_itm = req.body.txt_des_itm;
			/*
				Obligatorio
			*/

			/* Glosa descriptiva Detalle Adicional por Ítem (Opcional) */
			var txt_des_adic_itm = req.body.txt_des_adic_itm;
			/*
			*/

			/* Código de producto del ítem */
			var cod_itm = req.body.cod_itm;
			/*
				Opcional. Su uso será obligatorio si el emisor electrónico, opta por consignar este código, en reemplazo de la descripción detallada
			*/

			/* Código de producto SUNAT */
			var cod_itm_sunat = req.body.cod_itm_sunat;
			/*
				Opcional. Código del producto de acuerdo al estándar internacional de la ONU denominado: United Nations Standard
				Products and Services Code - Código de productos y servicios estándar de las Naciones Unidas
			*/

			/* Número de documento del huésped */
			var hpj_num_doc = req.body.hpj_num_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del tipo de documento del huésped */
			var hpj_tip_doc = req.body.hpj_tip_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del país de emisión del pasaporte */
			var hpj_pais_emi_pasp = req.body.hpj_pais_emi_pasp;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Nombres y apellidos o denominación social del huésped */
			var hpj_nom_ape_rzn_scl = req.body.hpj_nom_ape_rzn_scl;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del país de residencia del huésped */
			var hpj_pais_res = req.body.hpj_pais_res;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Número de días de permanencia */
			var hpj_num_dias_per = req.body.hpj_num_dias_per;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de ingreso al establecimiento */
			var hpj_fec_ing = req.body.hpj_fec_ing;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de salida al establecimiento */
			var hpj_fec_sal = req.body.hpj_fec_sal;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de consumo del huesped */
			var hpj_fec_csm = req.body.hpj_fec_csm;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Documento de identidad del huésped */
			var pqt_num_doc = req.body.pqt_num_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Codigo de tipo de documento del huésped */
			var pqt_tip_doc = req.body.pqt_tip_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Nombre del huésped */
			var pqt_nom_doc = req.body.pqt_nom_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Numero de Asientos */
			var tpt_num_asnt = req.body.tpt_num_asnt;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Valor del detalle adicional 01 */
			var det_val_adic01 = req.body.det_val_adic01;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 02 */
			var det_val_adic02 = req.body.det_val_adic02;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 03 */
			var det_val_adic03 = req.body.det_val_adic03;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 04 */
			var det_val_adic04 = req.body.det_val_adic04;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 05 */
			var det_val_adic05 = req.body.det_val_adic05;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 06 */
			var det_val_adic06 = req.body.det_val_adic06;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 07 */
			var det_val_adic07 = req.body.det_val_adic07;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 08 */
			var det_val_adic08 = req.body.det_val_adic08;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 09 */
			var det_val_adic09 = req.body.det_val_adic09;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 10 */
			var det_val_adic10 = req.body.det_val_adic10;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Descripcion del ITEM Complementaria */
			var des_comp = req.body.des_comp;
			/*	
			*/

			/*	FIN DE CARGA DE DATOS DETALLES	*/
			/*  INICIO VARIABLES DOCUMENTOS REFERENCIAS	*/

			/* Numero secuencial del Documento de Referencia */
			var num_lin_ref = req.body.num_lin_ref;
			/*	
				Solo de corresponder
			*/

			/* Código de Tipo de Documento de referencia */
			var des_comp = req.body.des_comp;
			/*	
							01 – Factura
							02 – Boleta de venta
							07 – Nota de crédito
							08 – Nota de débito
			*/

			/* Serie de Documento de Referencia. */
			var num_serie_cpe_ref = req.body.num_serie_cpe_ref;
			/*	
				Es obligatorio solo de corresponder
			*/

			/* Numero correlativo de documento referencia */
			var num_corre_cpe_ref = req.body.num_corre_cpe_ref;
			/*	
				Es obligatorio solo de corresponder
			*/

			/* Fecha de emision del documento de referencia */
			var fec_doc_ref = req.body.fec_doc_ref;
			/*	
				El formato debe ser AAAA-MM-DD
			*/

			/* Código de Tipo de Otro Documento de Referencia. */
			var cod_tip_otr_doc_ref = req.body.cod_tip_otr_doc_ref;
			/*	
								Solo de corresponder
								01 – Factura emitida para corregir error en el RUC
								02 – Factura emitida por anticipos
								03 – Boleta de venta emitida por anticipos
								04 – Ticket de salida ENAPU
								05 – código SCOP
								99 - Otros

								01 => FACTURA
								03 => BOLETA VENTA
								07 => NOTA CRED
								08 => NOTA DEB
			*/

			/* Número de Otro Documento de Referencia. */
			var num_otr_doc_ref = req.body.num_otr_doc_ref;
			/*	
				Solo de corresponder
			*/

			/* CAMPO DE ENVIO DE EMAIL */
			var email = req.body.email;
			
			var args = {
			"oUser": util.taxtech.user,
			"oPass": util.taxtech.pass,
			"oCabecera": {
					"ID": "876178c8fc875c949a0b7df5f716985d8338fa33e5a4017ebffb623ba8451d02",
					"COD_GPO": "1",
					"TIP_CPE": cod_tip_otr_doc_ref,
					"FEC_EMI": fec_emi,
					"HOR_EMI": hor_emi,
					"SERIE": serie,
					"CORRELATIVO": correlativo,
					"MONEDA": moneda,
					"COD_TIP_OPE": cod_tip_ope,
					"TIP_DOC_EMI": "6",
					"NRO_DOC_EMI": util.taxtech.ruc,
					"NOM_EMI": "TURIFAX SOCIEDAD ANONIMA CERRADA - TURIFAX S.A.C.",
					"NOM_COM_EMI": "HOLA MUNDO",
					"COD_LOC_EMI": "0000",
					"TIP_DOC_RCT": tip_doc_rct,
					"NRO_DOC_RCT": nro_doc_rct,
					"NOM_RCT": nom_rct,
					"DIR_DES_RCT": dir_des_rct,
					"UBI_RCT": "150101",
					"TIP_PAG": tip_pag,
					"FRM_PAG": frm_pag,
					"NRO_ORD_COM": "OC-235466",
					"FMT_IMPR": "000",
					"MNT_DCTO_GLB": "0.00",
					"FAC_DCTO_GLB": "0.00",
					"MNT_CARG_GLB": "0.00",
					"FAC_CARG_GLOB": "0.00",
					"MNT_TOT_PER": "0.00",
					"FAC_PER": "0.00",
					"MNT_TOT_IMP": mnt_tot_imp,
					"MNT_TOT_GRV": mnt_tot_grv,
					"MNT_TOT_INF": mnt_tot_inf,
					"MNT_TOT_EXR": mnt_tot_exr,
					"MNT_TOT_GRT": mnt_tot_grt,
					"MNT_TOT_EXP": mnt_tot_exp,
					"MNT_TOT_TRB_IGV": mnt_tot_trb_igv,
					"MNT_TOT_TRB_ISC": "0.00",
					"MNT_TOT_TRB_OTR": "0.00",
					"MNT_TOT_VAL_VTA": "0.00",
					"MNT_TOT_PRC_VTA": "0.00",
					"MNT_TOT_DCTO": "0.00",
					"MNT_TOT_OTR_CGO": "0.00",
					"MNT_TOT": mnt_tot,
					"MNT_TOT_ANTCP": "0.00",
					"MNT_DCTO_GLB_NAC": "0.00",
					"MNT_CARG_GLB_NAC": "0.00",
					"MNT_TOT_PER_NAC": "0.00",
					"MNT_TOT_IMP_NAC": "3.65",
					"MNT_TOT_GRV_NAC": "20.30",
					"MNT_TOT_INF_NAC": "0.00",
					"MNT_TOT_EXR_NAC": "0.00",
					"MNT_TOT_GRT_NAC": "0.00",
					"MNT_TOT_EXP_NAC": "0.00",
					"MNT_TOT_TRB_IGV_NAC": "3.65",
					"MNT_TOT_TRB_ISC_NAC": "0.00",
					"MNT_TOT_TRB_OTR_NAC": "0.00",
					"MNT_TOT_VAL_VTA_NAC": "0.00",
					"MNT_TOT_PRC_VTA_NAC": "0.00",
					"MNT_TOT_DCTO_NAC": "0.00",
					"MNT_TOT_OTR_CGO_NAC": "0.00",
					"MNT_TOT_NAC": "23.95",
					"MNT_TOT_ANTCP_NAC": "0.00",
					"COD_TIP_DETRACCION": "001",
					"MNT_TOT_DETRACCION": "80",
					"FAC_DETRACCION": "0.02",
					"COD_SOF_FACT": "COD001",
					"COD_TIP_ND":cod_tip_nd,
					"CORREO_ENVIO": email,
					"COD_PROCEDENCIA": "003",
					"ID_EXT_RZN": "1",
					"ETD_SNT": "101"
				},
				"listDetalle": {},
				"listAdicionales": {
					"CPE_DAT_ADIC_BE": [
						{
							"COD_TIP_ADIC_SUNAT": "38",
							"NUM_LIN_ADIC_SUNAT": "38",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "39",
							"NUM_LIN_ADIC_SUNAT": "39",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "40",
							"NUM_LIN_ADIC_SUNAT": "40",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "50",
							"NUM_LIN_ADIC_SUNAT": "50",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "60",
							"NUM_LIN_ADIC_SUNAT": "60",
							"TXT_DESC_ADIC_SUNAT": ""
						}
					]
				},
			"listReferenciados": null,
			"listAnticipos": null,
			"listFacGuia": null,
			"listRelacionado": null,
			"oTipoOnline": "Normal"
			};

			/* detalles de factura */
			var list_data = {};
			var list_service_doc_arr = [];
			if(req.body.hasOwnProperty("list_service_doc")){
				if(req.body.list_service_doc !== ""){
					var list_service_doc = JSON.parse(req.body.list_service_doc);
					console.log(list_service_doc);
					var igv = 0.18;
					for(var i = 0;i < list_service_doc.length;i++){
						//OBTENIENDO VARIABLES
						var current_deta = list_service_doc[i];
						var cod_itm = (current_deta.service_doc_trib !== undefined && current_deta.service_doc_trib !== "") ? current_deta.service_doc_trib : "";
						var cod_und_itm = (current_deta.cod_und_itm !== undefined && current_deta.cod_und_itm !== "") ? current_deta.cod_und_itm : "";
						var txt_des_itm = (current_deta.service_doc_name !== undefined && current_deta.service_doc_name !== "") ? current_deta.service_doc_name : "";
						var txt_des_adic_itm = (current_deta.service_doc_name_detail !== undefined && current_deta.service_doc_name_detail !== "") ? current_deta.service_doc_name_detail : "";
						var cant_und_itm = (current_deta.service_doc_quantity !== undefined && current_deta.service_doc_quantity !== "") ? parseInt(current_deta.service_doc_quantity) : 0.00;
						var val_vta_itm = (current_deta.service_doc_amount !== undefined && current_deta.service_doc_amount !== "") ? parseFloat(current_deta.service_doc_amount) : 0.00;
						var val_vta_brt_item =  (current_deta.val_vta_brt_item !== undefined && current_deta.val_vta_brt_item !== "") ? parseFloat(current_deta.val_vta_brt_item) : 0.00;
						var val_vta_itm_igv = val_vta_itm;
						var tributo = parseFloat(current_deta.service_doc_trib);

						//GENERANDO OBJETO
						var obj_service_doc = {};
						obj_service_doc.LIN_ITM = (i + 1);
						obj_service_doc.COD_UND_ITM = cod_und_itm;
						obj_service_doc.CANT_UND_ITM = cant_und_itm;
						obj_service_doc.VAL_VTA_ITM = parseFloat(val_vta_itm).toFixed(2); //val_vta_itm * cant_und_itm
						obj_service_doc.PRC_VTA_UND_ITM = parseFloat((val_vta_itm) + tributo).toFixed(2);
						obj_service_doc.VAL_UNIT_ITM = parseFloat(val_vta_itm_igv).toFixed(2);
						obj_service_doc.MNT_IGV_ITM = parseFloat(tributo).toFixed(2);
						obj_service_doc.POR_IGV_ITM = ((tributo)*100);
						//obj_service_doc.PRC_VTA_ITEM = parseFloat(((((val_vta_itm)*tributo)-(val_vta_itm))*(-1))*(cant_und_itm)).toFixed(2);
						obj_service_doc.PRC_VTA_ITEM = parseFloat((val_vta_itm) + tributo).toFixed(2);
						obj_service_doc.VAL_VTA_BRT_ITEM = parseFloat(val_vta_brt_item).toFixed(2);
						obj_service_doc.COD_TIP_AFECT_IGV_ITM = "10";
						obj_service_doc.TXT_DES_ITM = txt_des_itm;
						obj_service_doc.TXT_DES_ADIC_ITM = txt_des_adic_itm;
						obj_service_doc.COD_ITM = cod_itm;
						list_service_doc_arr.push(obj_service_doc);
					}
					list_data.CPE_DETALLE_BE = list_service_doc_arr;
					args.listDetalle = list_data;
				}
			}
			console.log("=============================");
			
			soap.createClient(url, function(err, client) {
			client.callProcessOnline(args, function(err, result) {
				if(err){
					return res.send(400, {'success':false,'data' : []});
				}else{
					res.send(200, {'success':true,'data' : result});
				}
			});
			});

		//}
	//});
});

router.post('/qa', function (req, res, next) {

	var cod_gpo = req.body.cod_gpo; 
	var id_taxtech = req.body.id_taxtech;

	var nro_doc = "";
	var query = document.getMaxNro(req.body);

	logger.trace('Entering cheese testing');
	logger.debug('Got cheese.');
	logger.info('Cheese is Comté.');
	logger.warn('Cheese is quite smelly.');
	logger.error('Cheese is too ripe!');
	logger.fatal('Cheese was breeding ground for listeria.');

	//util.connection.query(query, function (error, results){
		//if (!error) {
			//var url = 'http://facturaenuna.pe/OnlineCPE/CPE/wsOnlineToCPE.svc?wsdl'; //DESARROLLO
			var url = 'http://facturaenuna.com/OnlineCPE/CPE/wsOnlineToCPE.svc?wsdl'; //PRODUCCION
			var oUser = req.body.oUser; 
			var oPass = req.body.oPass; 
			var nro_doc_emi = req.body.nro_doc_emi;
			var nom_emi = req.body.nom_emi;
			var nom_com_emi = req.body.nom_com_emi;
			
			//PARAMETROS FACTURA

			/* FECHA DE EMISION DEL COMPROBANTE */
			var fec_emi = req.body.fec_emi; 
			/*
				2018-07-27
			*/
			
			/* HORA DE EMISION DEL COMPROBANTE */
			var hor_emi = req.body.hor_emi;
			/* 
				08:56:14
			*/

			/* NUMERO DE SERIE DEL COMPROBANTE */
			var serie = req.body.serie;
			/* 
				F004
			*/

			/* NUMERO DEL CORRELATIVO DEL DOCUMENTO */
			/*
			var correlativo = 0;
			if(results[0].nro !== undefined){
				correlativo = parseInt(results[0].nro) + 1; 
			}else{
				correlativo = req.body.correlativo; 
			}

			console.log("===================");
			console.log(results[0].nro);
			console.log("===================");
			
			*/

			var correlativo = req.body.correlativo; 
			/* 
				00000020
			*/

			/* TIPO DE MONEDA */
			var moneda = req.body.moneda;
			/* 
				PEN
			*/

			/* CODIGO DE TIPO DE OPERACION */
			var cod_tip_ope = req.body.cod_tip_ope;
			/*
				0101 - Venta Interna / 
				0102 - Venta Interna - Anticipos / 
				0103 - Venta Interna - Deducción de Anticipos / 
				0104 - Venta Itinerante
			*/

			/* Código de Tipo de Identificador Fiscal del Emisor */
			var tip_doc_emi = req.body.tip_doc_emi;
			/* 
				6
			*/
			/* Número de identificador fiscal del emisor (RUC) */
			var nro_doc = req.body.nro_doc;
			/*
				20601890659
			*/

			/* Razón Social del Emisor */
			var nom_emi = req.body.nom_emi;
			/*
				TAX TECHNOLOGY PRUEBA SAC
			*/

			/* Nombre Comercial del Emisor  */
			var nom_com_emi = req.body.nom_com_emi;
			/*
				TAXTECH SAC
			*/

			/* Código de Tipo de Número de Identificador de Receptor (CLIENTE) */
			var tip_doc_rct = req.body.tip_doc_rct;
			/*
				0 – Doc. Trib. No Dom. Sin RUC.
				1 – DNI
				4 – Carnet de extranjería
				6 – RUC
				7 – Pasaporte
				A – Cedula diplomática de identidad
				B – Doc. Identi. País residencia No Domiciliado Tratándose de
				operaciones de exportación (COD_TIP_OPE = 0102) el código a
				utilizar será “-“
			*/

			/* NRO DOC DEL CLIENTE // Tratándose de operaciones de exportación (COD_TIP_OPE = 0102)  */
			var nro_doc_rct = req.body.nro_doc_rct;
			/*
				20601890000
			*/

			/* NOM CLIENTE RECEPTOR */
			var nom_rct = req.body.nom_rct;
			/*
				CLIENTE RECEPTOR S.A.C.
			*/

			/* Razón Social del Receptor (CLIENTE) */
			var dir_des_rct = req.body.dir_des_rct;
			/*
				AV. COMANDANTE ESPINAR NRO. 435 DPTO. 803 CJRES PEDRO RUIZ GALLO
			*/

			/* CODIGO DE UBIGEO DE LA PROVINCIA | DEPARTAMENTO | DISTRITO */
			var ubi_rct = req.body.ubi_rct;
			/*
				150101
			*/

			/* CODIGO DEL TIPO DE PAGO */
			var tip_pag = req.body.tip_pag;
			/*
				000 => NO ASIGNADO
				001 => CONTADO
				002 => CRÉDITO A 7 DÍAS
				003 => CRÉDITO A 15 DÍAS
				004 => CRÉDITO A 30 DÍAS
				005 => CRÉDITO A 60 DÍAS
				006 => CRÉDITO A 90 DÍAS
				007 => CRÉDITO A 120 DÍAS
				008 => CRÉDITO A 20 DÍAS
				009 => CRÉDITO A 45 DÍAS
			*/

			/* CODIGO DE FORMA DE PAGO */
			var frm_pag = req.body.frm_pag;
			/*
				000 NO ASIGNADO
				001 EFECTIVO
				002 CHEQUE
				003 LETRA
				004 TARJETA DE CRÉDITO
				005 TARJETA DE DÉBITO
				006 DEPOSITO BANCARIO
				007 TRANSFERENCIA INTERBANCARIA
			*/

			/* NUMERO DE ORDEN DE COMPRA */
			var nro_ord_com = req.body.nro_ord_com;
			/*
				OC-235466
			*/

			/* Código del Tipo de Guía Texto */
			var cod_tip_gre = req.body.cod_tip_gre;
			/*
				Tipo Texto logitud de 2
			*/ 

			/* Numero del Tipo de Guía Texto */
			var nro_gre = req.body.nro_gre;
			/*
				Tipo Texto logitud de 15
			*/ 

			/* Enviar etiqueta vacia */
			var cod_opc = req.body.cod_opc;
			/*
				Tipo Texto logitud de 50
			*/ 

			/* Forma de impresion */
			var fmt_impr = req.body.fmt;
			/*
				Tipo Texto logitud de 3, siempre enviar el valor "001"
			*/ 

			/* Impresora destino */
			var impresora = req.body.impresora;
			/*
				Tipo Texto logitud de 120, Enviar “nombre o IP del servidor”/”nombre de la impresora”
				como está configurada en la RED, siempre que la instalación sea in House. Caso contrario se envía Vacío.
			*/ 

			/* Monto descuento global en moneda de la transaccion */
			var mnt_dcto_glb = req.body.mnt_dcto_glb;
			/*
				Tipo Numerico logitud de 12.2, Distinto al elemento Total Descuentos. Su propósito es permitir
				consignar en el comprobante de pago, un descuento a nivel global o total. Este campo no debe ser usado para contener la
				suma de los descuentos de línea o ítem.
			*/ 

			/* FACTOR DEL DESCUENTO -GLOBAL */
			var fac_dcto_glb = req.body.fac_dcto_glb;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* MONTO DE CARGO GLOBAL EN LA MONEDA DE LA TRANSACCION */
			var mnt_carg_glb = req.body.mnt_carg_glb;
			/*
				Distinto al elemento Total Cargos. Su propósito es permitir consignar en el comprobante de pago, un cargo a nivel global o
				total. Este campo no debe ser usado para contener la suma de los cargos de línea o ítem
			*/ 

			/* FACTOR DE CARGO GLOBAL */
			var fac_carg_glob = req.body.fac_carg_glb;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* TIPO DE CARGO GLOBAL */
			var tip_carg_glob = req.body.tip_carg_glob;
			/*
				Tipo Numerico logitud de 2, 50 – Otros cargos
											54 – Otros cargos relacionados al servicio
											55 – otros cargos no relacionados al servicio
			*/

			/* MONTO TOTAL PERCEPCION DE LA MONEDA DE LA TRANSACCION */
			var mnt_tot_per = req.body.mnt_tot_per;
			/*
				Tipo Numerico logitud de 12.2
			*/ 	

			/* CODIGO TIPO DE PERCEPCION */
			var tip_per = req.body.tip_per;
			/*
				Tipo Numerico logitud de 2, 51 – Percepción venta interna
											52 – Percepción a la adquisición de combustible
											53 - Percepción al agente de percepción con tasa especial
			*/  

			/* FACTOR DE PERCEPCION */
			var fac_per = req.body.fac_per;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* FACTOR DE PERCEPCION */
			var fac_per = req.body.fac_per;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* SUMATORIA TOTAL DE IMPUESTO EN MONEDA DE LA TRANSACCION */
			var mnt_tot_imp = req.body.mnt_tot_imp;
			/*
				Tipo Numerico logitud de 12.2, Corresponde a la suma de todos los impuestos ISC, IGV e IVAP
			*/ 

			/* SUMATORIA TOTAL DE VALORES DE VENTA */
			var mnt_tot_grv = req.body.mnt_tot_grv;
			/*
				Tipo Numerico logitud de 12.2
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES INAFECTAS */
			var mnt_tot_inf = req.body.mnt_tot_inf;
			/*
				Tipo Numerico logitud de 12.2, Contiene la informacion de la suma de todos los items por ventas inafectas
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES EXONERADAS */
			var mnt_tot_exr = req.body.mnt_tot_exr;
			/*
				Tipo Numerico logitud de 12.2, Contiene la informacion de la suma de todos los items por ventas inafectas
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES EXONERADAS */
			var mnt_tot_exe = req.body.mnt_tot_exe;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items exonerados
			*/ 

			/* SUMATORIA TOTAL GRATUITO */
			var mnt_tot_grt = req.body.mnt_tot_grt;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items gratuitos
			*/ 

			/* SUMATORIA TOTAL DEL VALOR DE VENTAS DE LAS OPERACIONES DE EXPORTACION */
			var mnt_tot_exp = req.body.mnt_tot_exp;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items exportaciones
			*/

			/* SUMATORIA TOTAL DE OPERACIONES AFECTAS A ISC */
			var mnt_tot_isc = req.body.mnt_tot_isc;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items ISC
			*/

			/* SUMATORIA TOTAL IGV */
			var mnt_tot_trb_igv = req.body.mnt_tot_trb_igv;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items IGV
			*/

			/* SUMATORIA TOTAL ISC */
			var mnt_tot_trb_isc = req.body.mnt_tot_trb_isc;
			/*
				Tipo Numerico logitud de 12.2, Corresponde al ISC total de la factura, la sumatorua no debe contener el ISC que corresponde a la transferencia de bienes
			*/

			/* SUMATORIA TOTAL DE OTROS TRIBUTOS EN SOLES */
			var mnt_tot_trb_otr = req.body.mnt_tot_trb_otr;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de otros tributos distinto al ISC y IGV
			*/

			/* MONTO TOTAL DE LA VENTA */
			var mnt_tot_val_vta = req.body.mnt_tot_val_vta;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items
			*/

			/* TOTAL PRECIO DE VENTA EN MONEDA DE LA TRANSACCION */
			var mnt_tot_prc_vta = req.body.mnt_tot_prc_vta;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total del valor de venta
			*/

			/* MONTO TOTAL DE DESCUENTO */
			var mnt_tot_dct = req.body.mnt_tot_dct;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los descuentos
			*/

			/* MONTO TOTAL DE OTROS CARGOS */
			var mnt_tot_otr_cgo = req.body.mnt_tot_otr_cgo;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de otros cargos
			*/

			/* MONTO TOTAL DE DESCUENTO */
			var mnt_tot_dcto = req.body.mnt_tot_dcto;
			/*

			/* IMPORTE TOTAL DE LA VENTA */
			var mnt_tot = parseFloat(req.body.mnt_tot).toFixed(2);
			/*
				Tipo Numerico logitud de 12.2, Suma de MON_TOT_PRC_VTA - MNT_TOT_DCT + MNT_TOT_OTR_CGO -
				menos los anticipos que hubieran sido recibidos
			*/

			/* MONTO TOTAL DE ANTICIPOS */
			var mnt_tot_antcp = req.body.mnt_tot_antcp;
			/*
				Tipo Numerico logitud de 12.2, Corresponde al total de anticipos del comprobante
			*/

			/* TIPO DE CAMBIO A CALCULAR A MONEDA NACIONAL */
			var tip_cmb = req.body.tip_cmb;
			/*
				Tipo Numerico logitud de 2.6, Enviar vacio en caso que el valor de moneda enviado sea soles (PEN)
			*/

			/* MONTO DESCUENTO GLOBAL DE MONEDA NACIONAL */
			var mnt_dcto_glb_nac = req.body.mnt_dcto_glb_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO CARGO GLOBAL EN MONEDA NACIONAL */
			var mnt_carg_glb_nac = req.body.mnt_carg_glb_nac;
			/*
				Tipo Numerico logitud de 12.2, Su proposito es consignar en el comprobante
			*/

			/* MONTO TOTAL DE PERCEPCION */
			var mnt_tot_per_nac = req.body.mnt_tot_per_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL IMPUESTOS NACIONAL */
			var mnt_tot_imp_nac = req.body.mnt_tot_imp_nac;
			/*
				Tipo Numerico logitud de 12.2, Corresonde a la suma total de todos los impuestos
			*/

			/* MONTO TOTAL GRAVADO */
			var mnt_tot_grv_nac = req.body.mnt_tot_grv_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL INAFECTO */
			var mnt_tot_inf_nac = req.body.mnt_tot_inf_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL EXONERADO */
			var mnt_tot_exr_nac = req.body.mnt_tot_exr_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL GRATUITO */
			var mnt_tot_grt_nac = req.body.mnt_tot_grt_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL EXPORTACION */
			var mnt_tot_exp_nac = req.body.mnt_tot_exp_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL IGV */
			var mnt_tot_trb_igv_nac = req.body.mnt_tot_trb_igv_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL ISC */
			var mnt_tot_trb_isc_nac = req.body.mnt_tot_trb_isc_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL OTROS TRIBUTOS */
			var mnt_tot_trb_otr_nac = (req.body.hasOwnProperty("mnt_tot_trb_otr_nac") && req.body.mnt_tot_trb_otr_nac !== undefined && req.body.mnt_tot_trb_otr_nac !== "") ? req.body.mnt_tot_trb_otr_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL VALOR DE VENTA */
			//var mnt_tot_val_vta_nac = req.body.mnt_tot_val_vta_nac;
			var mnt_tot_val_vta_nac = (req.body.hasOwnProperty("mnt_tot_val_vta_nac") && req.body.mnt_tot_val_vta_nac !== undefined && req.body.mnt_tot_val_vta_nac !== "") ? req.body.mnt_tot_val_vta_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL PRECIO DE VENTA */
			//var mnt_tot_prc_vta_nac = req.body.mnt_tot_prc_vta_nac;
			var mnt_tot_prc_vta_nac = (req.body.hasOwnProperty("mnt_tot_prc_vta_nac") && req.body.mnt_tot_prc_vta_nac !== undefined && req.body.mnt_tot_prc_vta_nac !== "") ? req.body.mnt_tot_prc_vta_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL DESCUENTO */
			//var mnt_tot_dcto_nac = req.body.mnt_tot_dcto_nac;
			var mnt_tot_dcto_nac = (req.body.hasOwnProperty("mnt_tot_dcto_nac") && req.body.mnt_tot_dcto_nac !== undefined && req.body.mnt_tot_dcto_nac !== "") ? req.body.mnt_tot_dcto_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL OTROS CARGOS */
			var mnt_tot_cgo_nac = req.body.mnt_tot_cgo_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL */
			var mnt_tot_nac = req.body.mnt_tot_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL ANTICIPO */
			var mnt_tot_antcp_nac = req.body.mnt_tot_antcp_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* CODIGO DEL BIEN O PRODUCTO */
			var cod_tip_detraccion = req.body.cod_tip_detraccion;
			/*
				Tipo Numerico logitud de 3, Otros servicios empresariales       "022"
											Arrendamiento de bienes muebles     "019"
											Demás servicios gravados con el IGV "037"
			*/

			/* MONTO TOTAL DETRACCION */
			var mnt_tot_detraccion = req.body.mnt_tot_detraccion;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* PORCENTAJE DETRACCION */
			var fac_detraccion = req.body.fac_detraccion;
			/*
				Tipo Numerico logitud de 3.2
			*/

			/* CODIGO DEL SOFTWARE DE FACTURACCION */
			var cod_sof_fact = req.body.cod_sof_fact;
			/*
				Tipo Texto logitud de 20
			*/

			/* CODIGO DE TIPO NOTA DE CREDITO */
			var cod_tip_nc = req.body.cod_tip_nc;
			/*
				Tipo Texto logitud de 2 (Dejar en blanco si el CPE no es Nota de Crédito)
											01 – Anulación de la operación
											02 – Anulación por error en el RUC
											03 – Corrección por error en la descripción
											04 – Descuento global (solo para factura)
											05 – Descuento por ítem (solo para factura)
											06 – Devolución total
											07 – Devolución por ítem
											08 – Bonificación (solo para factura)
											09 – Disminución en el valor
											10 – Otros conceptos
			*/

			/* CODIGO DE TIPO NOTA DE DEBITO */
			var cod_tip_nd = req.body.cod_tip_nd;
			/*
				Tipo Texto logitud de 2 (Dejar en blanco si el CPE no es Nota de Débito)
											01 – Intereses por mora
											02 – Aumento de valor
											03 – Penalidades
			*/

			/* TEXTO DE DESCRIPCION DEL MOTIVO CPE */
			var des_mtvo_nc_nd = req.body.des_mtvo_nc_nd;
			/*
				Tipo Texto logitud de 250
			*/

			/* CORREO DE ENVIO */
			var correo_envio = req.body.correo_envio;
			/*
				Tipo Texto logitud de 300
			*/

			/* CORREO COPIA */
			var correo_copia = req.body.correo_copia;
			/*
				Tipo Texto logitud de 300
			*/

			/* CORREO OCULTO */
			var correo_oculto = req.body.correo_oculto;
			/*
				Tipo Texto logitud de 300
			*/

			/* IDENTIFICADOR DEL PUNTO DE VENTA */
			var pto_vta = req.body.pto_vta;
			/*
				Tipo Texto logitud de 10
			*/

			/* FLAG DE TIPO DE CAMBIO */
			var flg_tip_cambio = req.body.flg_tip_cambio;
			/*
				Siempre enviar el valor "1"
			*/

			/* CODIGO SISTEMAS */
			var cod_procedencia = req.body.cod_procedencia;
			/*
				Siempre enviar el valor "002"
			*/

			/* CODIGO SISTEMAS */
			var id_ext_rzn = req.body.id_ext_rzn;
			/*
				Siempre enviar el valor "1"
			*/

			/* CODIGO SISTEMAS */
			var etd_snt = req.body.etd_snt;
			/*
				Enviar etiqueta vacia
			*/

			/* DESCRIPCION REFERENCIA DEL CLIENTE */
			var des_ref_clt = req.body.des_ref_clt;
			/*
				Enviar Vacio
			*/
		/* FIN VARIABLES DE CONTENIDO Y ENCABEZADO */

		/* INICIO VARIABLES DETALLE DE FACTURAS */

			/* NUMERO DEL CONSECUTIVO DEL ITEMS */
			var lin_itm = req.body.lin_itm;
			/*
				Numerico longitud 3, Campo obligatorio corresponde al numero de linea secuencial
			*/

			/* UNIDAD DE MEDIDA POR ITEMS */
			var cod_und_itm = req.body.cod_und_itm;
			/*
				Para la agencia corresponde ZZ
											BJ – Balde / BG – Bolsa / BO – Botellas / BX – Caja / CT –
											Cartones / CY – Cilindro / CJ – Conos / GRM – Gramo / SET –
											Juego / KGM – Kilogramo / KTM – Kilometro / KT – Kit / CA –
											Latas / LBR – Libras / LTR – Litro / MTR – Metro / MLL – Millares
											/
											NIU - Unidad (Bienes)
											ZZ - Unidad (Servicios)
			*/

			/* CANTIDAD DE UNIDADES POR ITEMS */
			var cant_und_itm = req.body.cant_und_itm;
			/*
				Numerico longitud 12.2, Cantidad de productos vendidos
			*/

			/* Valor de venta por línea/ítem (No incluye IGV, ISC, Otros Tributos) */
			var val_vta_itm = req.body.val_vta_itm;
			/*
				Numerico longitud 12.2, Obligatorio. Este elemento es el producto de la cantidad por el
				valor unitario (Q x Valor Unitario) y la deducción de descuentos aplicados a dicho ítem (de existir).
				Este importe no incluye los tributos (IGV, ISC y otros Tributos), los descuentos globales o cargos.
			*/

			/* Precio unitario de Venta por línea/ítem. (Incluye IGV, ISC y Otros tributos) */
			var prc_vta_und_itm = req.body.prc_vta_und_itm;
			/*
				Obligatorio. Dentro del ámbito tributario, es el monto correspondiente al precio unitario facturado del bien vendido o
				servicio vendido. Este monto es la suma total que queda obligado a pagar el adquirente o usuario por cada bien o
				servicio. Esto incluye los tributos (IGV, ISC y otros Tributos) y la deducción de descuentos por ítem		
			*/

			/* Valor unitario por ítem */
			var val_unit_itm = req.body.val_unit_itm;
			/*
				Valor de venta unitario por ítem, solo de corresponder
			*/

			/* Monto de IGV por ítem */
			var mnt_igv_itm = req.body.mnt_igv_itm;
			/*
				Es obligatorio solo de corresponder	
			*/

			/* Porcentaje de IGV por ítem */
			var por_igv_itm = req.body.por_igv_itm;
			/*
				Es obligatorio solo de corresponder	
			*/

			/* Importe de Venta por línea/ítem. (Incluye IGV, ISC y Otros tributos) */
			var prc_vta_item = req.body.prc_vta_item;
			/*
			*/

			/* Valor de Venta por Ítem antes de Aplicar Descuentos y Cargos. */
			var val_vta_brt_item = req.body.val_vta_brt_item;
			/*
				Si no existe Descuentos y Cargos , el valor debe ser igual a "VAL_VTA_ITEM".
			*/

			/* Código de tipo de Afectación al IGV por ítem */
			var cod_tip_afect_igv_itm = req.body.cod_tip_afect_igv_itm;
			/*
							10 – Gravado – operación onerosa
							11 – gravado – retiro por premio
							12 – gravado -retiro por donación
							14- gravado – retiro
							15 – gravado – bonificaciones
							16 – gravado- retiro por entrega a trabajadores
							17 – Gravado – IVAP
							20 – Exonerado – operación onerosa
							21 – Exonerado – operación onerosa
							30 – Inafecto – operación onerosa
							31 – Inafecto – retiro por bonificación
							32 – Inafecto – retiro
							33 – Inafecto – Retiro por muestras médicas
							34 – Inafecto – retiro por convenio colectivo
							35 – inafecto – retiro por premio
							36 – Inafecto – retiro por publicidad
							40 – exportación
			*/

			/* MONTO DE ISC POR ITEM */
			var mnt_isc_itm = req.body.mnt_isc_itm;
			/*
				Es obligatorio solo de corresponder
			*/

			/* PORCENTAJE DE ISC POR ITEM */
			var por_isc_itm = req.body.por_isc_itm;
			/*
				Es obligatorio solo de corresponder
			*/

			/* CODIGO DEL TIPO DE ISC */
			var cod_tip_sist_isc = req.body.cod_tip_sist_isc;
			/*
				se consigna el código de tipo de Sistema de ISC Aplicado, puede tomar los siguientes valores:
							01- Sistema al valor
							02- Aplicación del Monto Fijo
							03- Sistema de Precios de Venta al Público
			*/

			/* PRECIO SUGERIDO PARA ISC */
			var precio_sugerido_isc = req.body.precio_sugerido_isc;
			/*
				En los casos que no se tenga este dato, se debe pasar el valor venta del producto/servicio. Este dato multiplicado por el
				porcentaje del ISC se obtiene el Monto del ISC MNT_ISC_ITM= POR_ISC_ITM* PRECIO_SUGERIDO_ISC.
			*/

			/* MONTO DE DESCUENTO POR ITEM */
			var mnt_dcto_itm = req.body.mnt_dcto_itm;
			/*
			*/

			/* FACTOR DEL DESCUENTO POR ITEM */
			var fac_dcto_itm = req.body.fac_dcto_itm;
			/*
			*/

			/* MONTO DEL CARGO POR ITEM */
			var mnt_carg_itm = req.body.mnt_carg_itm;
			/*
			*/

			/* Factor del Cargo por ítem */
			var fac_carg_itm = req.body.fac_carg_itm;
			/*
				Debe expresarse en número, no en porcentaje. Ejemplo: si el porcentaje es 2%, consignar 0.02
			*/

			/* Tipo de Cargo por ítem */
			var tip_carg_itm = req.body.tip_carg_itm;
			/*
							50 – Otros cargos
							54 – Otros cargos relacionados al servicio
							55 – otros cargos no relacionados al servicio
			*/

			/* Monto Total Percepción por ítem en la Moneda de la Transacción */
			var mnt_tot_per_itm = req.body.mnt_tot_per_itm;
			/*
			*/

			/* Factor de la Percepción por ítem */
			var fac_per_itm = req.body.fac_per_itm;
			/*
				Debe expresarse en número, no en porcentaje. Ejemplo: si el porcentaje es 2%, consignar 0.02
			*/

			/* Código de Tipo de Percepción por ítem */
			var tip_per_itm = req.body.tip_per_itm;
			/*
								51 – Percepción venta interna
								52 – Percepción a la adquisición de combustible
								53 - Percepción al agente de percepción con tasa especial
			*/

			/* Descripción detallada del servicio prestado, bien vendido o cedido en uso, indicando el nombre y las características, tales como marca del bien vendido o cedido en uso. */
			var txt_des_itm = req.body.txt_des_itm;
			/*
				Obligatorio
			*/

			/* Glosa descriptiva Detalle Adicional por Ítem (Opcional) */
			var txt_des_adic_itm = req.body.txt_des_adic_itm;
			/*
			*/

			/* Código de producto del ítem */
			var cod_itm = req.body.cod_itm;
			/*
				Opcional. Su uso será obligatorio si el emisor electrónico, opta por consignar este código, en reemplazo de la descripción detallada
			*/

			/* Código de producto SUNAT */
			var cod_itm_sunat = req.body.cod_itm_sunat;
			/*
				Opcional. Código del producto de acuerdo al estándar internacional de la ONU denominado: United Nations Standard
				Products and Services Code - Código de productos y servicios estándar de las Naciones Unidas
			*/

			/* Número de documento del huésped */
			var hpj_num_doc = req.body.hpj_num_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del tipo de documento del huésped */
			var hpj_tip_doc = req.body.hpj_tip_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del país de emisión del pasaporte */
			var hpj_pais_emi_pasp = req.body.hpj_pais_emi_pasp;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Nombres y apellidos o denominación social del huésped */
			var hpj_nom_ape_rzn_scl = req.body.hpj_nom_ape_rzn_scl;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del país de residencia del huésped */
			var hpj_pais_res = req.body.hpj_pais_res;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Número de días de permanencia */
			var hpj_num_dias_per = req.body.hpj_num_dias_per;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de ingreso al establecimiento */
			var hpj_fec_ing = req.body.hpj_fec_ing;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de salida al establecimiento */
			var hpj_fec_sal = req.body.hpj_fec_sal;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de consumo del huesped */
			var hpj_fec_csm = req.body.hpj_fec_csm;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Documento de identidad del huésped */
			var pqt_num_doc = req.body.pqt_num_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Codigo de tipo de documento del huésped */
			var pqt_tip_doc = req.body.pqt_tip_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Nombre del huésped */
			var pqt_nom_doc = req.body.pqt_nom_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Numero de Asientos */
			var tpt_num_asnt = req.body.tpt_num_asnt;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Valor del detalle adicional 01 */
			var det_val_adic01 = req.body.det_val_adic01;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 02 */
			var det_val_adic02 = req.body.det_val_adic02;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 03 */
			var det_val_adic03 = req.body.det_val_adic03;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 04 */
			var det_val_adic04 = req.body.det_val_adic04;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 05 */
			var det_val_adic05 = req.body.det_val_adic05;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 06 */
			var det_val_adic06 = req.body.det_val_adic06;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 07 */
			var det_val_adic07 = req.body.det_val_adic07;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 08 */
			var det_val_adic08 = req.body.det_val_adic08;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 09 */
			var det_val_adic09 = req.body.det_val_adic09;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 10 */
			var det_val_adic10 = req.body.det_val_adic10;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Descripcion del ITEM Complementaria */
			var des_comp = req.body.des_comp;
			/*	
			*/

			/*	FIN DE CARGA DE DATOS DETALLES	*/
			/*  INICIO VARIABLES DOCUMENTOS REFERENCIAS	*/

			/* Numero secuencial del Documento de Referencia */
			var num_lin_ref = req.body.num_lin_ref;
			/*	
				Solo de corresponder
			*/

			/* Código de Tipo de Documento de referencia */
			var des_comp = req.body.des_comp;
			/*	
							01 – Factura
							02 – Boleta de venta
							07 – Nota de crédito
							08 – Nota de débito
			*/

			/* Serie de Documento de Referencia. */
			var num_serie_cpe_ref = req.body.num_serie_cpe_ref;
			/*	
				Es obligatorio solo de corresponder
			*/

			/* Numero correlativo de documento referencia */
			var num_corre_cpe_ref = req.body.num_corre_cpe_ref;
			/*	
				Es obligatorio solo de corresponder
			*/

			/* Fecha de emision del documento de referencia */
			var fec_doc_ref = req.body.fec_doc_ref;
			/*	
				El formato debe ser AAAA-MM-DD
			*/

			/* Código de Tipo de Otro Documento de Referencia. */
			var cod_tip_otr_doc_ref = req.body.cod_tip_otr_doc_ref;
			/*	
								Solo de corresponder
								01 – Factura emitida para corregir error en el RUC
								02 – Factura emitida por anticipos
								03 – Boleta de venta emitida por anticipos
								04 – Ticket de salida ENAPU
								05 – código SCOP
								99 - Otros

								01 => FACTURA
								03 => BOLETA VENTA
								07 => NOTA CRED
								08 => NOTA DEB
			*/

			/* Número de Otro Documento de Referencia. */
			var num_otr_doc_ref = req.body.num_otr_doc_ref;
			/*	
				Solo de corresponder
			*/

			/*		vValores de Notas de Credito y Debito
			*/
            var des_mtvo_nc_nd = req.body.des_mtvo_nc_nd;
            var cod_tip_doc_ref = req.body.cod_tip_doc_ref;
            var num_serie_cpe_ref = req.body.num_serie_cpe_ref;
        	var num_corre_cpe_ref = req.body.num_corre_cpe_ref;
        	var fec_doc_ref = req.body.fec_doc_ref;
        	var cod_tip_nc = req.body.cod_tip_nc;
        	var cod_tip_nd = req.body.cod_tip_nd;
        	var tip_cpe = req.body.tip_cpe;
        	var serie_corre_cpe_ref = req.body.serie_corre_cpe_ref;


			/* CAMPO DE ENVIO DE EMAIL */
			var email = req.body.email;
			
			var args = {
			"oUser": oUser,//util.taxtech.user
			"oPass": oPass,//util.taxtech.pass
			"oCabecera": {
					"ID": id_taxtech, // Valor suministrado por Taxtech
					"COD_GPO": cod_gpo, // Valor suministrado por Taxtech
					"TIP_CPE": tip_cpe,
					"FEC_EMI": fec_emi,
					"HOR_EMI": hor_emi,
					"SERIE": serie,
					"CORRELATIVO": correlativo,
					"MONEDA": moneda,
					"COD_TIP_OPE": cod_tip_ope,
					"TIP_DOC_EMI": "6",
					"NRO_DOC_EMI": nro_doc_emi,//util.taxtech.ruc
					"NOM_EMI": nom_emi,
					"NOM_COM_EMI": nom_com_emi,
					"COD_LOC_EMI": "0000",
					"TIP_DOC_RCT": tip_doc_rct,
					"NRO_DOC_RCT": nro_doc_rct,
					"NOM_RCT": nom_rct,
					"DIR_DES_RCT": dir_des_rct,
					"UBI_RCT": "150101",
					"TIP_PAG": tip_pag,
					"FRM_PAG": frm_pag,
					"NRO_ORD_COM": "OC-235466",
					"FMT_IMPR": "001",
					"MNT_DCTO_GLB": mnt_dcto_glb,
					"FAC_DCTO_GLB": fac_dcto_glb,
					"MNT_CARG_GLB": mnt_carg_glb,
					"FAC_CARG_GLOB": fac_carg_glob,
					"MNT_TOT_PER": mnt_tot_per,
					"FAC_PER": fac_per,
					"MNT_TOT_IMP": mnt_tot_imp,
					"MNT_TOT_GRV": mnt_tot_grv,
					"MNT_TOT_INF": mnt_tot_inf,
					"MNT_TOT_EXR": mnt_tot_exr,
					"MNT_TOT_GRT": mnt_tot_grt,
					"MNT_TOT_EXP": mnt_tot_exp,
					"MNT_TOT_TRB_IGV": mnt_tot_imp,
					"MNT_TOT_TRB_ISC": mnt_tot_trb_isc,
					"MNT_TOT_TRB_OTR": mnt_tot_trb_otr,
					"MNT_TOT_VAL_VTA": mnt_tot_val_vta,
					"MNT_TOT_PRC_VTA": mnt_tot_prc_vta,
					"MNT_TOT_DCTO": mnt_tot_dcto,
					"MNT_TOT_OTR_CGO": mnt_tot_otr_cgo,
					"MNT_TOT": mnt_tot,
					"MNT_TOT_ANTCP": mnt_tot_antcp,
					"MNT_DCTO_GLB_NAC": mnt_dcto_glb_nac,
					"MNT_CARG_GLB_NAC": mnt_carg_glb_nac,
					"MNT_TOT_PER_NAC": mnt_tot_per_nac,
					"MNT_TOT_IMP_NAC": mnt_tot_imp_nac,
					"MNT_TOT_GRV_NAC": mnt_tot_grv_nac,
					"MNT_TOT_INF_NAC": mnt_tot_inf_nac,
					"MNT_TOT_EXR_NAC": mnt_tot_exr_nac,
					"MNT_TOT_GRT_NAC": mnt_tot_grt_nac,
					"MNT_TOT_EXP_NAC": mnt_tot_exp_nac,
					"MNT_TOT_TRB_IGV_NAC": mnt_tot_trb_igv_nac,
					"MNT_TOT_TRB_ISC_NAC": mnt_tot_trb_isc_nac,
					"MNT_TOT_TRB_OTR_NAC": mnt_tot_trb_otr_nac,
					"MNT_TOT_VAL_VTA_NAC": mnt_tot_val_vta_nac,
					"MNT_TOT_PRC_VTA_NAC": mnt_tot_prc_vta_nac,
					"MNT_TOT_DCTO_NAC": mnt_tot_dcto_nac,
					"MNT_TOT_OTR_CGO_NAC": "0.00",
					"MNT_TOT_NAC": mnt_tot_nac,
					"MNT_TOT_ANTCP_NAC": mnt_tot_antcp_nac,
					"COD_TIP_DETRACCION": cod_tip_detraccion,
					"MNT_TOT_DETRACCION": mnt_tot_detraccion,
					"FAC_DETRACCION": fac_detraccion,
					"COD_SOF_FACT": "COD001",
	        		"COD_TIP_NC": cod_tip_nc,
	        		"COD_TIP_ND": cod_tip_nd,
				    "DES_MTVO_NC_ND": des_mtvo_nc_nd,
					"CORREO_ENVIO": email,
					"COD_PROCEDENCIA": "003",
					"ID_EXT_RZN": "1",
					"ETD_SNT": "101"

				},

				"listDetalle": {},

				"listAdicionales": {
					"CPE_DAT_ADIC_BE": [
						{
							"COD_TIP_ADIC_SUNAT": "38",
							"NUM_LIN_ADIC_SUNAT": "38",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "39",
							"NUM_LIN_ADIC_SUNAT": "39",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "40",
							"NUM_LIN_ADIC_SUNAT": "40",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "50",
							"NUM_LIN_ADIC_SUNAT": "50",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "60",
							"NUM_LIN_ADIC_SUNAT": "60",
							"TXT_DESC_ADIC_SUNAT": ""
						}
					]
				},

				"listReferenciados": {
					"CPE_DOC_REF_BE": 
						{
	                    "COD_TIP_DOC_REF": cod_tip_doc_ref,
	        			"COD_TIP_OTR_DOC_REF": cod_tip_otr_doc_ref,
	        			"FEC_DOC_REF": fec_doc_ref,
	        			"NUM_CORRE_CPE_REF": num_corre_cpe_ref,
						"NUM_LIN_REF": "001",
						"NUM_OTR_DOC_REF": num_otr_doc_ref,
						"NUM_SERIE_CPE_REF": num_serie_cpe_ref,
	        			"SERIE_CORRE_CPE_REF": serie_corre_cpe_ref,
	        			"NUM_OTR_DOC_REF": num_otr_doc_ref
						}
				},

			"listAnticipos": null,
			"listFacGuia": null,
			"listRelacionado": null,
			"oTipoOnline": "Normal"
			};

			/* detalles de factura */
			var list_data = {};
			var list_service_doc_arr = [];
			if(req.body.hasOwnProperty("list_service_doc")){
				if(req.body.list_service_doc !== ""){
					var list_service_doc = JSON.parse(req.body.list_service_doc);
					console.log(list_service_doc);
					for(var i = 0;i < list_service_doc.length;i++){
						//OBTENIENDO VARIABLES
						var current_deta = list_service_doc[i];
						var cod_itm = (current_deta.cod_itm !== undefined && current_deta.cod_itm !== "") ? parseFloat(current_deta.cod_itm) : 0.00;
						var cod_und_itm = (current_deta.cod_und_itm !== undefined && current_deta.cod_und_itm !== "") ? current_deta.cod_und_itm : "";
						var txt_des_itm = (current_deta.txt_des_itm !== undefined && current_deta.txt_des_itm !== "") ? current_deta.txt_des_itm : "";
						var txt_des_adic_itm = (current_deta.txt_des_adic_itm !== undefined && current_deta.txt_des_adic_itm !== "") ? current_deta.txt_des_adic_itm : "";
						var cant_und_itm = (current_deta.cant_und_itm !== undefined && current_deta.cant_und_itm !== "") ? parseInt(current_deta.cant_und_itm) : 0;
						var val_vta_itm = (current_deta.val_vta_itm !== undefined && current_deta.val_vta_itm !== "") ? parseFloat(current_deta.val_vta_itm) : 0.00;
						var prc_vta_und_itm = (current_deta.prc_vta_und_itm !== undefined && current_deta.prc_vta_und_itm !== "") ? parseFloat(current_deta.prc_vta_und_itm) : 0.00;
						var prc_vta_item = (current_deta.prc_vta_item !== undefined && current_deta.prc_vta_item !== "") ? parseFloat(current_deta.prc_vta_item) : 0.00;
						var cod_tip_afect_igv_itm = (current_deta.cod_tip_afect_igv_itm !== undefined && current_deta.cod_tip_afect_igv_itm !== "") ? parseFloat(current_deta.cod_tip_afect_igv_itm) : 0.00;
						var por_igv_itm = (current_deta.por_igv_itm !== undefined && current_deta.por_igv_itm !== "") ? parseFloat(current_deta.por_igv_itm) : 0.00;
						var val_vta_brt_item =  (current_deta.val_vta_brt_item !== undefined && current_deta.val_vta_brt_item !== "") ? parseFloat(current_deta.val_vta_brt_item) : 0.00;
						//var val_vta_itm_igv = val_vta_itm;
						var tributo = parseFloat(current_deta.service_doc_trib).toFixed(2);

						//===============================================
						var val_unit_itm = (current_deta.val_unit_itm !== undefined && current_deta.val_unit_itm !== "") ? parseFloat(current_deta.val_unit_itm) : 0.00;
						var mnt_igv_itm = (current_deta.mnt_igv_itm !== undefined && current_deta.mnt_igv_itm !== "") ? parseFloat(current_deta.mnt_igv_itm) : 0.00;

						//GENERANDO OBJETO
						var obj_service_doc = {};
						obj_service_doc.LIN_ITM = (i + 1);
						obj_service_doc.COD_UND_ITM = cod_und_itm;
						obj_service_doc.CANT_UND_ITM = cant_und_itm;
						obj_service_doc.VAL_VTA_ITM = val_vta_itm; //val_vta_itm * cant_und_itm
						obj_service_doc.PRC_VTA_UND_ITM = parseFloat(prc_vta_und_itm).toFixed(2);
						obj_service_doc.VAL_UNIT_ITM = parseFloat(val_unit_itm).toFixed(2);
						obj_service_doc.MNT_IGV_ITM = mnt_igv_itm;
						obj_service_doc.POR_IGV_ITM = por_igv_itm;
						//obj_service_doc.PRC_VTA_ITEM = parseFloat(((((val_vta_itm)*tributo)-(val_vta_itm))*(-1))*(cant_und_itm)).toFixed(2);
						obj_service_doc.PRC_VTA_ITEM = prc_vta_item;
						obj_service_doc.VAL_VTA_BRT_ITEM = val_vta_brt_item;
						obj_service_doc.COD_TIP_AFECT_IGV_ITM = cod_tip_afect_igv_itm;
						obj_service_doc.TXT_DES_ITM = txt_des_itm;
						obj_service_doc.TXT_DES_ADIC_ITM = txt_des_adic_itm;
						obj_service_doc.COD_ITM = cod_itm;
						list_service_doc_arr.push(obj_service_doc);
					}
					list_data.CPE_DETALLE_BE = list_service_doc_arr;
					args.listDetalle = list_data;
				}
			}			
			logger.error("================================= // =============================================");
			logger.error(args);
			logger.error("================================= // =============================================");
			soap.createClient(url, function(err, client) {
			client.callProcessOnline(args, function(err, result) {
				if(err){
					return res.send(400, {'success':false,'data' : []});
				}else{
					res.send(200, {'success':true,'data' : result});
				}
			});
			});

		//}
	//});
});

router.post('/prod/taxtech', function (req, res, next) {


	var cod_gpo = req.body.cod_gpo; 
	var id_taxtech = req.body.id_taxtech;

	var nro_doc = "";
	var query = document.getMaxNro(req.body);

	logger.error("================================= // =============================================");
	logger.trace('Entering cheese testing');
	logger.debug('Got cheese.');
	logger.info('Cheese is Comté.');
	logger.warn('Cheese is quite smelly.');
	logger.error('Cheese is too ripe!');
	logger.fatal('Cheese was breeding ground for listeria.');

	//util.connection.query(query, function (error, results){
		//if (!error) {
			//var url = 'http://facturaenuna.pe/OnlineCPE/CPE/wsOnlineToCPE.svc?wsdl'; //DESARROLLO
			var url = 'http://facturaenuna.com/OnlineCPE/CPE/wsOnlineToCPE.svc?wsdl'; //PRODUCCION
			var oUser = req.body.oUser; 
			var oPass = req.body.oPass; 
			var nro_doc_emi = req.body.nro_doc_emi;
			var nom_emi = req.body.nom_emi;
			var nom_com_emi = req.body.nom_com_emi;
			
			//PARAMETROS FACTURA

			/* FECHA DE EMISION DEL COMPROBANTE */
			var fec_emi = req.body.fec_emi; 
			/*
				2018-07-27
			*/
			
			/* HORA DE EMISION DEL COMPROBANTE */
			var hor_emi = req.body.hor_emi;
			/* 
				08:56:14
			*/

			/* NUMERO DE SERIE DEL COMPROBANTE */
			var serie = req.body.serie;
			/* 
				F004
			*/

			/* NUMERO DEL CORRELATIVO DEL DOCUMENTO */
			/*
			var correlativo = 0;
			if(results[0].nro !== undefined){
				correlativo = parseInt(results[0].nro) + 1; 
			}else{
				correlativo = req.body.correlativo; 
			}

			console.log("===================");
			console.log(results[0].nro);
			console.log("===================");
			
			*/

			var correlativo = req.body.correlativo; 
			/* 
				00000020
			*/

			/* TIPO DE MONEDA */
			var moneda = req.body.moneda;
			/* 
				PEN
			*/

			/* CODIGO DE TIPO DE OPERACION */
			var cod_tip_ope = req.body.cod_tip_ope;
			/*
				0101 - Venta Interna / 
				0102 - Venta Interna - Anticipos / 
				0103 - Venta Interna - Deducción de Anticipos / 
				0104 - Venta Itinerante
			*/

			/* Código de Tipo de Identificador Fiscal del Emisor */
			var tip_doc_emi = req.body.tip_doc_emi;
			/* 
				6
			*/
			/* Número de identificador fiscal del emisor (RUC) */
			var nro_doc = req.body.nro_doc;
			/*
				20601890659
			*/

			/* Razón Social del Emisor */
			var nom_emi = req.body.nom_emi;
			/*
				TAX TECHNOLOGY PRUEBA SAC
			*/

			/* Nombre Comercial del Emisor  */
			var nom_com_emi = req.body.nom_com_emi;
			/*
				TAXTECH SAC
			*/

			/* Código de Tipo de Número de Identificador de Receptor (CLIENTE) */
			var tip_doc_rct = req.body.tip_doc_rct;
			/*
				0 – Doc. Trib. No Dom. Sin RUC.
				1 – DNI
				4 – Carnet de extranjería
				6 – RUC
				7 – Pasaporte
				A – Cedula diplomática de identidad
				B – Doc. Identi. País residencia No Domiciliado Tratándose de
				operaciones de exportación (COD_TIP_OPE = 0102) el código a
				utilizar será “-“
			*/

			/* NRO DOC DEL CLIENTE // Tratándose de operaciones de exportación (COD_TIP_OPE = 0102)  */
			var nro_doc_rct = req.body.nro_doc_rct;
			/*
				20601890000
			*/

			/* NOM CLIENTE RECEPTOR */
			var nom_rct = req.body.nom_rct;
			/*
				CLIENTE RECEPTOR S.A.C.
			*/

			/* Razón Social del Receptor (CLIENTE) */
			var dir_des_rct = req.body.dir_des_rct;
			/*
				AV. COMANDANTE ESPINAR NRO. 435 DPTO. 803 CJRES PEDRO RUIZ GALLO
			*/

			/* CODIGO DE UBIGEO DE LA PROVINCIA | DEPARTAMENTO | DISTRITO */
			var ubi_rct = req.body.ubi_rct;
			/*
				150101
			*/

			/* CODIGO DEL TIPO DE PAGO */
			var tip_pag = req.body.tip_pag;
			/*
				000 => NO ASIGNADO
				001 => CONTADO
				002 => CRÉDITO A 7 DÍAS
				003 => CRÉDITO A 15 DÍAS
				004 => CRÉDITO A 30 DÍAS
				005 => CRÉDITO A 60 DÍAS
				006 => CRÉDITO A 90 DÍAS
				007 => CRÉDITO A 120 DÍAS
				008 => CRÉDITO A 20 DÍAS
				009 => CRÉDITO A 45 DÍAS
			*/

			/* CODIGO DE FORMA DE PAGO */
			var frm_pag = req.body.frm_pag;
			/*
				000 NO ASIGNADO
				001 EFECTIVO
				002 CHEQUE
				003 LETRA
				004 TARJETA DE CRÉDITO
				005 TARJETA DE DÉBITO
				006 DEPOSITO BANCARIO
				007 TRANSFERENCIA INTERBANCARIA
			*/

			/* NUMERO DE ORDEN DE COMPRA */
			var nro_ord_com = req.body.nro_ord_com;
			/*
				OC-235466
			*/

			/* Código del Tipo de Guía Texto */
			var cod_tip_gre = req.body.cod_tip_gre;
			/*
				Tipo Texto logitud de 2
			*/ 

			/* Numero del Tipo de Guía Texto */
			var nro_gre = req.body.nro_gre;
			/*
				Tipo Texto logitud de 15
			*/ 

			/* Enviar etiqueta vacia */
			var cod_opc = req.body.cod_opc;
			/*
				Tipo Texto logitud de 50
			*/ 

			/* Forma de impresion */
			var fmt_impr = req.body.fmt;
			/*
				Tipo Texto logitud de 3, siempre enviar el valor "001"
			*/ 

			/* Impresora destino */
			var impresora = req.body.impresora;
			/*
				Tipo Texto logitud de 120, Enviar “nombre o IP del servidor”/”nombre de la impresora”
				como está configurada en la RED, siempre que la instalación sea in House. Caso contrario se envía Vacío.
			*/ 

			/* Monto descuento global en moneda de la transaccion */
			var mnt_dcto_glb = req.body.mnt_dcto_glb;
			/*
				Tipo Numerico logitud de 12.2, Distinto al elemento Total Descuentos. Su propósito es permitir
				consignar en el comprobante de pago, un descuento a nivel global o total. Este campo no debe ser usado para contener la
				suma de los descuentos de línea o ítem.
			*/ 

			/* FACTOR DEL DESCUENTO -GLOBAL */
			var fac_dcto_glb = req.body.fac_dcto_glb;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* MONTO DE CARGO GLOBAL EN LA MONEDA DE LA TRANSACCION */
			var mnt_carg_glb = req.body.mnt_carg_glb;
			/*
				Distinto al elemento Total Cargos. Su propósito es permitir consignar en el comprobante de pago, un cargo a nivel global o
				total. Este campo no debe ser usado para contener la suma de los cargos de línea o ítem
			*/ 

			/* FACTOR DE CARGO GLOBAL */
			var fac_carg_glob = req.body.fac_carg_glb;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* TIPO DE CARGO GLOBAL */
			var tip_carg_glob = req.body.tip_carg_glob;
			/*
				Tipo Numerico logitud de 2, 50 – Otros cargos
											54 – Otros cargos relacionados al servicio
											55 – otros cargos no relacionados al servicio
			*/

			/* MONTO TOTAL PERCEPCION DE LA MONEDA DE LA TRANSACCION */
			var mnt_tot_per = req.body.mnt_tot_per;
			/*
				Tipo Numerico logitud de 12.2
			*/ 	

			/* CODIGO TIPO DE PERCEPCION */
			var tip_per = req.body.tip_per;
			/*
				Tipo Numerico logitud de 2, 51 – Percepción venta interna
											52 – Percepción a la adquisición de combustible
											53 - Percepción al agente de percepción con tasa especial
			*/  

			/* FACTOR DE PERCEPCION */
			var fac_per = req.body.fac_per;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* FACTOR DE PERCEPCION */
			var fac_per = req.body.fac_per;
			/*
				Tipo Numerico logitud de 3.5, Debe expresarse en numero no en porcentaje Ej: 0.02
			*/ 

			/* SUMATORIA TOTAL DE IMPUESTO EN MONEDA DE LA TRANSACCION */
			var mnt_tot_imp = req.body.mnt_tot_imp;
			/*
				Tipo Numerico logitud de 12.2, Corresponde a la suma de todos los impuestos ISC, IGV e IVAP
			*/ 

			/* SUMATORIA TOTAL DE VALORES DE VENTA */
			var mnt_tot_grv = req.body.mnt_tot_grv;
			/*
				Tipo Numerico logitud de 12.2
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES INAFECTAS */
			var mnt_tot_inf = req.body.mnt_tot_inf;
			/*
				Tipo Numerico logitud de 12.2, Contiene la informacion de la suma de todos los items por ventas inafectas
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES EXONERADAS */
			var mnt_tot_exr = req.body.mnt_tot_exr;
			/*
				Tipo Numerico logitud de 12.2, Contiene la informacion de la suma de todos los items por ventas inafectas
			*/ 

			/* SUMATORIA TOTAL DE OPERACIONES EXONERADAS */
			var mnt_tot_exe = req.body.mnt_tot_exe;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items exonerados
			*/ 

			/* SUMATORIA TOTAL GRATUITO */
			var mnt_tot_grt = req.body.mnt_tot_grt;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items gratuitos
			*/ 

			/* SUMATORIA TOTAL DEL VALOR DE VENTAS DE LAS OPERACIONES DE EXPORTACION */
			var mnt_tot_exp = req.body.mnt_tot_exp;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items exportaciones
			*/

			/* SUMATORIA TOTAL DE OPERACIONES AFECTAS A ISC */
			var mnt_tot_isc = req.body.mnt_tot_isc;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items ISC
			*/

			/* SUMATORIA TOTAL IGV */
			var mnt_tot_trb_igv = req.body.mnt_tot_trb_igv;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items IGV
			*/

			/* SUMATORIA TOTAL ISC */
			var mnt_tot_trb_isc = req.body.mnt_tot_trb_isc;
			/*
				Tipo Numerico logitud de 12.2, Corresponde al ISC total de la factura, la sumatorua no debe contener el ISC que corresponde a la transferencia de bienes
			*/

			/* SUMATORIA TOTAL DE OTROS TRIBUTOS EN SOLES */
			var mnt_tot_trb_otr = req.body.mnt_tot_trb_otr;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de otros tributos distinto al ISC y IGV
			*/

			/* MONTO TOTAL DE LA VENTA */
			var mnt_tot_val_vta = req.body.mnt_tot_val_vta;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los items
			*/

			/* TOTAL PRECIO DE VENTA EN MONEDA DE LA TRANSACCION */
			var mnt_tot_prc_vta = req.body.mnt_tot_prc_vta;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total del valor de venta
			*/

			/* MONTO TOTAL DE DESCUENTO */
			var mnt_tot_dct = req.body.mnt_tot_dct;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de los descuentos
			*/

			/* MONTO TOTAL DE OTROS CARGOS */
			var mnt_tot_otr_cgo = req.body.mnt_tot_otr_cgo;
			/*
				Tipo Numerico logitud de 12.2, Contiene la suma total de otros cargos
			*/

			/* MONTO TOTAL DE DESCUENTO */
			var mnt_tot_dcto = req.body.mnt_tot_dcto;
			/*

			/* IMPORTE TOTAL DE LA VENTA */
			var mnt_tot = parseFloat(req.body.mnt_tot).toFixed(2);
			/*
				Tipo Numerico logitud de 12.2, Suma de MON_TOT_PRC_VTA - MNT_TOT_DCT + MNT_TOT_OTR_CGO -
				menos los anticipos que hubieran sido recibidos
			*/

			/* MONTO TOTAL DE ANTICIPOS */
			var mnt_tot_antcp = req.body.mnt_tot_antcp;
			/*
				Tipo Numerico logitud de 12.2, Corresponde al total de anticipos del comprobante
			*/

			/* TIPO DE CAMBIO A CALCULAR A MONEDA NACIONAL */
			var tip_cmb = req.body.tip_cmb;
			/*
				Tipo Numerico logitud de 2.6, Enviar vacio en caso que el valor de moneda enviado sea soles (PEN)
			*/

			/* MONTO DESCUENTO GLOBAL DE MONEDA NACIONAL */
			var mnt_dcto_glb_nac = req.body.mnt_dcto_glb_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO CARGO GLOBAL EN MONEDA NACIONAL */
			var mnt_carg_glb_nac = req.body.mnt_carg_glb_nac;
			/*
				Tipo Numerico logitud de 12.2, Su proposito es consignar en el comprobante
			*/

			/* MONTO TOTAL DE PERCEPCION */
			var mnt_tot_per_nac = req.body.mnt_tot_per_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL IMPUESTOS NACIONAL */
			var mnt_tot_imp_nac = req.body.mnt_tot_imp_nac;
			/*
				Tipo Numerico logitud de 12.2, Corresonde a la suma total de todos los impuestos
			*/

			/* MONTO TOTAL GRAVADO */
			var mnt_tot_grv_nac = req.body.mnt_tot_grv_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL INAFECTO */
			var mnt_tot_inf_nac = req.body.mnt_tot_inf_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL EXONERADO */
			var mnt_tot_exr_nac = req.body.mnt_tot_exr_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL GRATUITO */
			var mnt_tot_grt_nac = req.body.mnt_tot_grt_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL EXPORTACION */
			var mnt_tot_exp_nac = req.body.mnt_tot_exp_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL IGV */
			var mnt_tot_trb_igv_nac = req.body.mnt_tot_trb_igv_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL ISC */
			var mnt_tot_trb_isc_nac = req.body.mnt_tot_trb_isc_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL OTROS TRIBUTOS */
			var mnt_tot_trb_otr_nac = (req.body.hasOwnProperty("mnt_tot_trb_otr_nac") && req.body.mnt_tot_trb_otr_nac !== undefined && req.body.mnt_tot_trb_otr_nac !== "") ? req.body.mnt_tot_trb_otr_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL VALOR DE VENTA */
			//var mnt_tot_val_vta_nac = req.body.mnt_tot_val_vta_nac;
			var mnt_tot_val_vta_nac = (req.body.hasOwnProperty("mnt_tot_val_vta_nac") && req.body.mnt_tot_val_vta_nac !== undefined && req.body.mnt_tot_val_vta_nac !== "") ? req.body.mnt_tot_val_vta_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL PRECIO DE VENTA */
			//var mnt_tot_prc_vta_nac = req.body.mnt_tot_prc_vta_nac;
			var mnt_tot_prc_vta_nac = (req.body.hasOwnProperty("mnt_tot_prc_vta_nac") && req.body.mnt_tot_prc_vta_nac !== undefined && req.body.mnt_tot_prc_vta_nac !== "") ? req.body.mnt_tot_prc_vta_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL DESCUENTO */
			//var mnt_tot_dcto_nac = req.body.mnt_tot_dcto_nac;
			var mnt_tot_dcto_nac = (req.body.hasOwnProperty("mnt_tot_dcto_nac") && req.body.mnt_tot_dcto_nac !== undefined && req.body.mnt_tot_dcto_nac !== "") ? req.body.mnt_tot_dcto_nac : 0.00;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL OTROS CARGOS */
			var mnt_tot_cgo_nac = req.body.mnt_tot_cgo_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL */
			var mnt_tot_nac = req.body.mnt_tot_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* MONTO TOTAL ANTICIPO */
			var mnt_tot_antcp_nac = req.body.mnt_tot_antcp_nac;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* CODIGO DEL BIEN O PRODUCTO */
			var cod_tip_detraccion = req.body.cod_tip_detraccion;
			/*
				Tipo Numerico logitud de 3, Otros servicios empresariales       "022"
											Arrendamiento de bienes muebles     "019"
											Demás servicios gravados con el IGV "037"
			*/

			/* MONTO TOTAL DETRACCION */
			var mnt_tot_detraccion = req.body.mnt_tot_detraccion;
			/*
				Tipo Numerico logitud de 12.2
			*/

			/* PORCENTAJE DETRACCION */
			var fac_detraccion = req.body.fac_detraccion;
			/*
				Tipo Numerico logitud de 3.2
			*/

			/* CODIGO DEL SOFTWARE DE FACTURACCION */
			var cod_sof_fact = req.body.cod_sof_fact;
			/*
				Tipo Texto logitud de 20
			*/

			/* CODIGO DE TIPO NOTA DE CREDITO */
			var cod_tip_nc = req.body.cod_tip_nc;
			/*
				Tipo Texto logitud de 2 (Dejar en blanco si el CPE no es Nota de Crédito)
											01 – Anulación de la operación
											02 – Anulación por error en el RUC
											03 – Corrección por error en la descripción
											04 – Descuento global (solo para factura)
											05 – Descuento por ítem (solo para factura)
											06 – Devolución total
											07 – Devolución por ítem
											08 – Bonificación (solo para factura)
											09 – Disminución en el valor
											10 – Otros conceptos
			*/

			/* CODIGO DE TIPO NOTA DE DEBITO */
			var cod_tip_nd = req.body.cod_tip_nd;
			/*
				Tipo Texto logitud de 2 (Dejar en blanco si el CPE no es Nota de Débito)
											01 – Intereses por mora
											02 – Aumento de valor
											03 – Penalidades
			*/

			/* TEXTO DE DESCRIPCION DEL MOTIVO CPE */
			var des_mtvo_nc_nd = req.body.des_mtvo_nc_nd;
			/*
				Tipo Texto logitud de 250
			*/

			/* CORREO DE ENVIO */
			var correo_envio = req.body.correo_envio;
			/*
				Tipo Texto logitud de 300
			*/

			/* CORREO COPIA */
			var correo_copia = req.body.correo_copia;
			/*
				Tipo Texto logitud de 300
			*/

			/* CORREO OCULTO */
			var correo_oculto = req.body.correo_oculto;
			/*
				Tipo Texto logitud de 300
			*/

			/* IDENTIFICADOR DEL PUNTO DE VENTA */
			var pto_vta = req.body.pto_vta;
			/*
				Tipo Texto logitud de 10
			*/

			/* FLAG DE TIPO DE CAMBIO */
			var flg_tip_cambio = req.body.flg_tip_cambio;
			/*
				Siempre enviar el valor "1"
			*/

			/* CODIGO SISTEMAS */
			var cod_procedencia = req.body.cod_procedencia;
			/*
				Siempre enviar el valor "002"
			*/

			/* CODIGO SISTEMAS */
			var id_ext_rzn = req.body.id_ext_rzn;
			/*
				Siempre enviar el valor "1"
			*/

			/* CODIGO SISTEMAS */
			var etd_snt = req.body.etd_snt;
			/*
				Enviar etiqueta vacia
			*/

			/* DESCRIPCION REFERENCIA DEL CLIENTE */
			var des_ref_clt = req.body.des_ref_clt;
			/*
				Enviar Vacio
			*/
		/* FIN VARIABLES DE CONTENIDO Y ENCABEZADO */

		/* INICIO VARIABLES DETALLE DE FACTURAS */

			/* NUMERO DEL CONSECUTIVO DEL ITEMS */
			var lin_itm = req.body.lin_itm;
			/*
				Numerico longitud 3, Campo obligatorio corresponde al numero de linea secuencial
			*/

			/* UNIDAD DE MEDIDA POR ITEMS */
			var cod_und_itm = req.body.cod_und_itm;
			/*
				Para la agencia corresponde ZZ
											BJ – Balde / BG – Bolsa / BO – Botellas / BX – Caja / CT –
											Cartones / CY – Cilindro / CJ – Conos / GRM – Gramo / SET –
											Juego / KGM – Kilogramo / KTM – Kilometro / KT – Kit / CA –
											Latas / LBR – Libras / LTR – Litro / MTR – Metro / MLL – Millares
											/
											NIU - Unidad (Bienes)
											ZZ - Unidad (Servicios)
			*/

			/* CANTIDAD DE UNIDADES POR ITEMS */
			var cant_und_itm = req.body.cant_und_itm;
			/*
				Numerico longitud 12.2, Cantidad de productos vendidos
			*/

			/* Valor de venta por línea/ítem (No incluye IGV, ISC, Otros Tributos) */
			var val_vta_itm = req.body.val_vta_itm;
			/*
				Numerico longitud 12.2, Obligatorio. Este elemento es el producto de la cantidad por el
				valor unitario (Q x Valor Unitario) y la deducción de descuentos aplicados a dicho ítem (de existir).
				Este importe no incluye los tributos (IGV, ISC y otros Tributos), los descuentos globales o cargos.
			*/

			/* Precio unitario de Venta por línea/ítem. (Incluye IGV, ISC y Otros tributos) */
			var prc_vta_und_itm = req.body.prc_vta_und_itm;
			/*
				Obligatorio. Dentro del ámbito tributario, es el monto correspondiente al precio unitario facturado del bien vendido o
				servicio vendido. Este monto es la suma total que queda obligado a pagar el adquirente o usuario por cada bien o
				servicio. Esto incluye los tributos (IGV, ISC y otros Tributos) y la deducción de descuentos por ítem		
			*/

			/* Valor unitario por ítem */
			var val_unit_itm = req.body.val_unit_itm;
			/*
				Valor de venta unitario por ítem, solo de corresponder
			*/

			/* Monto de IGV por ítem */
			var mnt_igv_itm = req.body.mnt_igv_itm;
			/*
				Es obligatorio solo de corresponder	
			*/

			/* Porcentaje de IGV por ítem */
			var por_igv_itm = req.body.por_igv_itm;
			/*
				Es obligatorio solo de corresponder	
			*/

			/* Importe de Venta por línea/ítem. (Incluye IGV, ISC y Otros tributos) */
			var prc_vta_item = req.body.prc_vta_item;
			/*
			*/

			/* Valor de Venta por Ítem antes de Aplicar Descuentos y Cargos. */
			var val_vta_brt_item = req.body.val_vta_brt_item;
			/*
				Si no existe Descuentos y Cargos , el valor debe ser igual a "VAL_VTA_ITEM".
			*/

			/* Código de tipo de Afectación al IGV por ítem */
			var cod_tip_afect_igv_itm = req.body.cod_tip_afect_igv_itm;
			/*
							10 – Gravado – operación onerosa
							11 – gravado – retiro por premio
							12 – gravado -retiro por donación
							14- gravado – retiro
							15 – gravado – bonificaciones
							16 – gravado- retiro por entrega a trabajadores
							17 – Gravado – IVAP
							20 – Exonerado – operación onerosa
							21 – Exonerado – operación onerosa
							30 – Inafecto – operación onerosa
							31 – Inafecto – retiro por bonificación
							32 – Inafecto – retiro
							33 – Inafecto – Retiro por muestras médicas
							34 – Inafecto – retiro por convenio colectivo
							35 – inafecto – retiro por premio
							36 – Inafecto – retiro por publicidad
							40 – exportación
			*/

			/* MONTO DE ISC POR ITEM */
			var mnt_isc_itm = req.body.mnt_isc_itm;
			/*
				Es obligatorio solo de corresponder
			*/

			/* PORCENTAJE DE ISC POR ITEM */
			var por_isc_itm = req.body.por_isc_itm;
			/*
				Es obligatorio solo de corresponder
			*/

			/* CODIGO DEL TIPO DE ISC */
			var cod_tip_sist_isc = req.body.cod_tip_sist_isc;
			/*
				se consigna el código de tipo de Sistema de ISC Aplicado, puede tomar los siguientes valores:
							01- Sistema al valor
							02- Aplicación del Monto Fijo
							03- Sistema de Precios de Venta al Público
			*/

			/* PRECIO SUGERIDO PARA ISC */
			var precio_sugerido_isc = req.body.precio_sugerido_isc;
			/*
				En los casos que no se tenga este dato, se debe pasar el valor venta del producto/servicio. Este dato multiplicado por el
				porcentaje del ISC se obtiene el Monto del ISC MNT_ISC_ITM= POR_ISC_ITM* PRECIO_SUGERIDO_ISC.
			*/

			/* MONTO DE DESCUENTO POR ITEM */
			var mnt_dcto_itm = req.body.mnt_dcto_itm;
			/*
			*/

			/* FACTOR DEL DESCUENTO POR ITEM */
			var fac_dcto_itm = req.body.fac_dcto_itm;
			/*
			*/

			/* MONTO DEL CARGO POR ITEM */
			var mnt_carg_itm = req.body.mnt_carg_itm;
			/*
			*/

			/* Factor del Cargo por ítem */
			var fac_carg_itm = req.body.fac_carg_itm;
			/*
				Debe expresarse en número, no en porcentaje. Ejemplo: si el porcentaje es 2%, consignar 0.02
			*/

			/* Tipo de Cargo por ítem */
			var tip_carg_itm = req.body.tip_carg_itm;
			/*
							50 – Otros cargos
							54 – Otros cargos relacionados al servicio
							55 – otros cargos no relacionados al servicio
			*/

			/* Monto Total Percepción por ítem en la Moneda de la Transacción */
			var mnt_tot_per_itm = req.body.mnt_tot_per_itm;
			/*
			*/

			/* Factor de la Percepción por ítem */
			var fac_per_itm = req.body.fac_per_itm;
			/*
				Debe expresarse en número, no en porcentaje. Ejemplo: si el porcentaje es 2%, consignar 0.02
			*/

			/* Código de Tipo de Percepción por ítem */
			var tip_per_itm = req.body.tip_per_itm;
			/*
								51 – Percepción venta interna
								52 – Percepción a la adquisición de combustible
								53 - Percepción al agente de percepción con tasa especial
			*/

			/* Descripción detallada del servicio prestado, bien vendido o cedido en uso, indicando el nombre y las características, tales como marca del bien vendido o cedido en uso. */
			var txt_des_itm = req.body.txt_des_itm;
			/*
				Obligatorio
			*/

			/* Glosa descriptiva Detalle Adicional por Ítem (Opcional) */
			var txt_des_adic_itm = req.body.txt_des_adic_itm;
			/*
			*/

			/* Código de producto del ítem */
			var cod_itm = req.body.cod_itm;
			/*
				Opcional. Su uso será obligatorio si el emisor electrónico, opta por consignar este código, en reemplazo de la descripción detallada
			*/

			/* Código de producto SUNAT */
			var cod_itm_sunat = req.body.cod_itm_sunat;
			/*
				Opcional. Código del producto de acuerdo al estándar internacional de la ONU denominado: United Nations Standard
				Products and Services Code - Código de productos y servicios estándar de las Naciones Unidas
			*/

			/* Número de documento del huésped */
			var hpj_num_doc = req.body.hpj_num_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del tipo de documento del huésped */
			var hpj_tip_doc = req.body.hpj_tip_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del país de emisión del pasaporte */
			var hpj_pais_emi_pasp = req.body.hpj_pais_emi_pasp;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Nombres y apellidos o denominación social del huésped */
			var hpj_nom_ape_rzn_scl = req.body.hpj_nom_ape_rzn_scl;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Código del país de residencia del huésped */
			var hpj_pais_res = req.body.hpj_pais_res;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Número de días de permanencia */
			var hpj_num_dias_per = req.body.hpj_num_dias_per;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de ingreso al establecimiento */
			var hpj_fec_ing = req.body.hpj_fec_ing;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de salida al establecimiento */
			var hpj_fec_sal = req.body.hpj_fec_sal;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Fecha de consumo del huesped */
			var hpj_fec_csm = req.body.hpj_fec_csm;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Documento de identidad del huésped */
			var pqt_num_doc = req.body.pqt_num_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Codigo de tipo de documento del huésped */
			var pqt_tip_doc = req.body.pqt_tip_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Paquete turístico - Nombre del huésped */
			var pqt_nom_doc = req.body.pqt_nom_doc;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Numero de Asientos */
			var tpt_num_asnt = req.body.tpt_num_asnt;
			/*
				Datos de establecimiento de Hospedaje
			*/

			/* Valor del detalle adicional 01 */
			var det_val_adic01 = req.body.det_val_adic01;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 02 */
			var det_val_adic02 = req.body.det_val_adic02;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 03 */
			var det_val_adic03 = req.body.det_val_adic03;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 04 */
			var det_val_adic04 = req.body.det_val_adic04;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 05 */
			var det_val_adic05 = req.body.det_val_adic05;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 06 */
			var det_val_adic06 = req.body.det_val_adic06;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 07 */
			var det_val_adic07 = req.body.det_val_adic07;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 08 */
			var det_val_adic08 = req.body.det_val_adic08;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 09 */
			var det_val_adic09 = req.body.det_val_adic09;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Valor del detalle adicional 10 */
			var det_val_adic10 = req.body.det_val_adic10;
			/*
				Las variables adicionales sirven para enviar información no tributaria a nivel detalle. Se tienen 10 variables adicionales por
				cada ítem del documento.
			*/

			/* Descripcion del ITEM Complementaria */
			var des_comp = req.body.des_comp;
			/*	
			*/

			/*	FIN DE CARGA DE DATOS DETALLES	*/
			/*  INICIO VARIABLES DOCUMENTOS REFERENCIAS	*/

			/* Numero secuencial del Documento de Referencia */
			var num_lin_ref = req.body.num_lin_ref;
			/*	
				Solo de corresponder
			*/

			/* Código de Tipo de Documento de referencia */
			var des_comp = req.body.des_comp;
			/*	
							01 – Factura
							02 – Boleta de venta
							07 – Nota de crédito
							08 – Nota de débito
			*/

			/* Serie de Documento de Referencia. */
			var num_serie_cpe_ref = req.body.num_serie_cpe_ref;
			/*	
				Es obligatorio solo de corresponder
			*/

			/* Numero correlativo de documento referencia */
			var num_corre_cpe_ref = req.body.num_corre_cpe_ref;
			/*	
				Es obligatorio solo de corresponder
			*/

			/* Fecha de emision del documento de referencia */
			var fec_doc_ref = req.body.fec_doc_ref;
			/*	
				El formato debe ser AAAA-MM-DD
			*/

			/* Código de Tipo de Otro Documento de Referencia. */
			var cod_tip_otr_doc_ref = req.body.cod_tip_otr_doc_ref;
			/*	
								Solo de corresponder
								01 – Factura emitida para corregir error en el RUC
								02 – Factura emitida por anticipos
								03 – Boleta de venta emitida por anticipos
								04 – Ticket de salida ENAPU
								05 – código SCOP
								99 - Otros

								01 => FACTURA
								03 => BOLETA VENTA
								07 => NOTA CRED
								08 => NOTA DEB
			*/

			/* Número de Otro Documento de Referencia. */
			var num_otr_doc_ref = req.body.num_otr_doc_ref;
			/*	
				Solo de corresponder
			*/

			/*		vValores de Notas de Credito y Debito
			*/
            var des_mtvo_nc_nd = req.body.des_mtvo_nc_nd;
            var cod_tip_doc_ref = req.body.cod_tip_doc_ref;
            var num_serie_cpe_ref = req.body.num_serie_cpe_ref;
        	var num_corre_cpe_ref = req.body.num_corre_cpe_ref;
        	var fec_doc_ref = req.body.fec_doc_ref;
        	var cod_tip_nc = req.body.cod_tip_nc;
        	var cod_tip_nd = req.body.cod_tip_nd;
        	var tip_cpe = req.body.tip_cpe;
        	var serie_corre_cpe_ref = req.body.serie_corre_cpe_ref;


			/* CAMPO DE ENVIO DE EMAIL */
			var email = req.body.email;
			
			var args = {
			"oUser": oUser,//util.taxtech.user
			"oPass": oPass,//util.taxtech.pass
			"oCabecera": {
					"ID": id_taxtech, // Valor suministrado por Taxtech
					"COD_GPO": cod_gpo, // Valor suministrado por Taxtech
					"TIP_CPE": tip_cpe,
					"FEC_EMI": fec_emi,
					"HOR_EMI": hor_emi,
					"SERIE": serie,
					"CORRELATIVO": correlativo,
					"MONEDA": moneda,
					"COD_TIP_OPE": cod_tip_ope,
					"TIP_DOC_EMI": "6",
					"NRO_DOC_EMI": nro_doc_emi,//util.taxtech.ruc
					"NOM_EMI": nom_emi,
					"NOM_COM_EMI": nom_com_emi,
					"COD_LOC_EMI": "0000",
					"TIP_DOC_RCT": tip_doc_rct,
					"NRO_DOC_RCT": nro_doc_rct,
					"NOM_RCT": nom_rct,
					"DIR_DES_RCT": dir_des_rct,
					"UBI_RCT": "150101",
					"TIP_PAG": tip_pag,
					"FRM_PAG": frm_pag,
					"NRO_ORD_COM": "OC-235466",
					"FMT_IMPR": "001",
					"MNT_DCTO_GLB": mnt_dcto_glb,
					"FAC_DCTO_GLB": fac_dcto_glb,
					"MNT_CARG_GLB": mnt_carg_glb,
					"FAC_CARG_GLOB": fac_carg_glob,
					"MNT_TOT_PER": mnt_tot_per,
					"FAC_PER": fac_per,
					"MNT_TOT_IMP": mnt_tot_imp,
					"MNT_TOT_GRV": mnt_tot_grv,
					"MNT_TOT_INF": mnt_tot_inf,
					"MNT_TOT_EXR": mnt_tot_exr,
					"MNT_TOT_GRT": mnt_tot_grt,
					"MNT_TOT_EXP": mnt_tot_exp,
					"MNT_TOT_TRB_IGV": mnt_tot_imp,
					"MNT_TOT_TRB_ISC": mnt_tot_trb_isc,
					"MNT_TOT_TRB_OTR": mnt_tot_trb_otr,
					"MNT_TOT_VAL_VTA": mnt_tot_val_vta,
					"MNT_TOT_PRC_VTA": mnt_tot_prc_vta,
					"MNT_TOT_DCTO": mnt_tot_dcto,
					"MNT_TOT_OTR_CGO": mnt_tot_otr_cgo,
					"MNT_TOT": mnt_tot,
					"MNT_TOT_ANTCP": mnt_tot_antcp,
					"MNT_DCTO_GLB_NAC": mnt_dcto_glb_nac,
					"MNT_CARG_GLB_NAC": mnt_carg_glb_nac,
					"MNT_TOT_PER_NAC": mnt_tot_per_nac,
					"MNT_TOT_IMP_NAC": mnt_tot_imp_nac,
					"MNT_TOT_GRV_NAC": mnt_tot_grv_nac,
					"MNT_TOT_INF_NAC": mnt_tot_inf_nac,
					"MNT_TOT_EXR_NAC": mnt_tot_exr_nac,
					"MNT_TOT_GRT_NAC": mnt_tot_grt_nac,
					"MNT_TOT_EXP_NAC": mnt_tot_exp_nac,
					"MNT_TOT_TRB_IGV_NAC": mnt_tot_trb_igv_nac,
					"MNT_TOT_TRB_ISC_NAC": mnt_tot_trb_isc_nac,
					"MNT_TOT_TRB_OTR_NAC": mnt_tot_trb_otr_nac,
					"MNT_TOT_VAL_VTA_NAC": mnt_tot_val_vta_nac,
					"MNT_TOT_PRC_VTA_NAC": mnt_tot_prc_vta_nac,
					"MNT_TOT_DCTO_NAC": mnt_tot_dcto_nac,
					"MNT_TOT_OTR_CGO_NAC": "0.00",
					"MNT_TOT_NAC": mnt_tot_nac,
					"MNT_TOT_ANTCP_NAC": mnt_tot_antcp_nac,
					"COD_TIP_DETRACCION": cod_tip_detraccion,
					"MNT_TOT_DETRACCION": mnt_tot_detraccion,
					"FAC_DETRACCION": fac_detraccion,
					"COD_SOF_FACT": "COD001",
	        		"COD_TIP_NC": cod_tip_nc,
	        		"COD_TIP_ND": cod_tip_nd,
				    "DES_MTVO_NC_ND": des_mtvo_nc_nd,
					"CORREO_ENVIO": email,
					"COD_PROCEDENCIA": "003",
					"ID_EXT_RZN": "1",
					"ETD_SNT": "101"

				},

				"listDetalle": {},

				"listAdicionales": {
					"CPE_DAT_ADIC_BE": [
						{
							"COD_TIP_ADIC_SUNAT": "38",
							"NUM_LIN_ADIC_SUNAT": "38",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "39",
							"NUM_LIN_ADIC_SUNAT": "39",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "40",
							"NUM_LIN_ADIC_SUNAT": "40",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "50",
							"NUM_LIN_ADIC_SUNAT": "50",
							"TXT_DESC_ADIC_SUNAT": ""
						},
						{
							"COD_TIP_ADIC_SUNAT": "60",
							"NUM_LIN_ADIC_SUNAT": "60",
							"TXT_DESC_ADIC_SUNAT": ""
						}
					]
				},

				"listReferenciados": {
					"CPE_DOC_REF_BE": 
						{
	                    "COD_TIP_DOC_REF": cod_tip_doc_ref,
	        			"COD_TIP_OTR_DOC_REF": cod_tip_otr_doc_ref,
	        			"FEC_DOC_REF": fec_doc_ref,
	        			"NUM_CORRE_CPE_REF": num_corre_cpe_ref,
						"NUM_LIN_REF": "001",
						"NUM_OTR_DOC_REF": num_otr_doc_ref,
						"NUM_SERIE_CPE_REF": num_serie_cpe_ref,
	        			"SERIE_CORRE_CPE_REF": serie_corre_cpe_ref,
	        			"NUM_OTR_DOC_REF": num_otr_doc_ref
						}
				},

			"listAnticipos": null,
			"listFacGuia": null,
			"listRelacionado": null,
			"oTipoOnline": "Normal"
			};

			/* detalles de factura */
			var list_data = {};
			var list_service_doc_arr = [];
			if(req.body.hasOwnProperty("list_service_doc")){
				if(req.body.list_service_doc !== ""){
					var list_service_doc = JSON.parse(req.body.list_service_doc);
					console.log(list_service_doc);
					for(var i = 0;i < list_service_doc.length;i++){
						//OBTENIENDO VARIABLES
						var current_deta = list_service_doc[i];
						var cod_itm = (current_deta.cod_itm !== undefined && current_deta.cod_itm !== "") ? parseFloat(current_deta.cod_itm) : 0.00;
						var cod_und_itm = (current_deta.cod_und_itm !== undefined && current_deta.cod_und_itm !== "") ? current_deta.cod_und_itm : "";
						var txt_des_itm = (current_deta.txt_des_itm !== undefined && current_deta.txt_des_itm !== "") ? current_deta.txt_des_itm : "";
						var txt_des_adic_itm = (current_deta.txt_des_adic_itm !== undefined && current_deta.txt_des_adic_itm !== "") ? current_deta.txt_des_adic_itm : "";
						var cant_und_itm = (current_deta.cant_und_itm !== undefined && current_deta.cant_und_itm !== "") ? parseInt(current_deta.cant_und_itm) : 0;
						var val_vta_itm = (current_deta.val_vta_itm !== undefined && current_deta.val_vta_itm !== "") ? parseFloat(current_deta.val_vta_itm) : 0.00;
						var prc_vta_und_itm = (current_deta.prc_vta_und_itm !== undefined && current_deta.prc_vta_und_itm !== "") ? parseFloat(current_deta.prc_vta_und_itm) : 0.00;
						var prc_vta_item = (current_deta.prc_vta_item !== undefined && current_deta.prc_vta_item !== "") ? parseFloat(current_deta.prc_vta_item) : 0.00;
						var cod_tip_afect_igv_itm = (current_deta.cod_tip_afect_igv_itm !== undefined && current_deta.cod_tip_afect_igv_itm !== "") ? parseFloat(current_deta.cod_tip_afect_igv_itm) : 0.00;
						var por_igv_itm = (current_deta.por_igv_itm !== undefined && current_deta.por_igv_itm !== "") ? parseFloat(current_deta.por_igv_itm) : 0.00;
						var val_vta_brt_item =  (current_deta.val_vta_brt_item !== undefined && current_deta.val_vta_brt_item !== "") ? parseFloat(current_deta.val_vta_brt_item) : 0.00;
						//var val_vta_itm_igv = val_vta_itm;
						var tributo = parseFloat(current_deta.service_doc_trib).toFixed(2);

						//===============================================
						var val_unit_itm = (current_deta.val_unit_itm !== undefined && current_deta.val_unit_itm !== "") ? parseFloat(current_deta.val_unit_itm) : 0.00;
						var mnt_igv_itm = (current_deta.mnt_igv_itm !== undefined && current_deta.mnt_igv_itm !== "") ? parseFloat(current_deta.mnt_igv_itm) : 0.00;

						//GENERANDO OBJETO
						var obj_service_doc = {};
						obj_service_doc.LIN_ITM = (i + 1);
						obj_service_doc.COD_UND_ITM = cod_und_itm;
						obj_service_doc.CANT_UND_ITM = cant_und_itm;
						obj_service_doc.VAL_VTA_ITM = val_vta_itm; //val_vta_itm * cant_und_itm
						obj_service_doc.PRC_VTA_UND_ITM = parseFloat(prc_vta_und_itm).toFixed(2);
						obj_service_doc.VAL_UNIT_ITM = parseFloat(val_unit_itm).toFixed(2);
						obj_service_doc.MNT_IGV_ITM = mnt_igv_itm;
						obj_service_doc.POR_IGV_ITM = por_igv_itm;
						//obj_service_doc.PRC_VTA_ITEM = parseFloat(((((val_vta_itm)*tributo)-(val_vta_itm))*(-1))*(cant_und_itm)).toFixed(2);
						obj_service_doc.PRC_VTA_ITEM = prc_vta_item;
						obj_service_doc.VAL_VTA_BRT_ITEM = val_vta_brt_item;
						obj_service_doc.COD_TIP_AFECT_IGV_ITM = cod_tip_afect_igv_itm;
						obj_service_doc.TXT_DES_ITM = txt_des_itm;
						obj_service_doc.TXT_DES_ADIC_ITM = txt_des_adic_itm;
						obj_service_doc.COD_ITM = cod_itm;
						list_service_doc_arr.push(obj_service_doc);
					}
					list_data.CPE_DETALLE_BE = list_service_doc_arr;
					args.listDetalle = list_data;
				}
			}			

			logger.error(args);
			
			soap.createClient(url, function(err, client) {
			client.callProcessOnline(args, function(err, result) {
				if(err){
					return res.send(400, {'success':false,'data' : []});
				}else{
					res.send(200, {'success':true,'data' : result});
				}
			});
		});
		logger.error("================================= // =============================================");
		//}
	//});
});

module.exports = router;