var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var employee = require('../models/employee');
var util = require('../util/util');

var self = {
	employee : {},
	list : [],
  	limit:10
};

/* SERVICIO DE PRODUCTOS */
router.get(util.api+'/employees', function (req, res, next) {
  var query = employee.getQuery(req.query);
  util.connection.query(query, function (error, results){
  	if (error) {
  		console.log(error);
		return res.send(400, {'success':false,'data' : []});
  	}else{
		if(results.length > 0){
	        var response = self.getList(results);
	        return res.send(200, {'success':true,'data' : response});
      	}else{
        	return res.send({'success':false,'data' : []});
      	}
  	}
  });
});

/* =================== FUNCIONES DE SERVICIO ============================= */
//LISTADO DE PRODUCTOS
self.getList = function(data){
	if(data.length > 0){
		self.list = [];
		for (var i = 0; i < data.length; i++) {
			self.list.push({
		        'person_id' : data[i].person_id,
		        'email' : data[i].email,
		        'first_name' : data[i].first_name,
		        'last_name' : data[i].last_name
			});
		}
	}
	return self.list;
};

module.exports = router;