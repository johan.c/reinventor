var express = require('express');
var router = express.Router();
var pg = require('pg');

pg.defaults.ssl = process.env.DATABASE_URL != undefined;
var conString = process.env.DATABASE_URL || "postgres://diana_db:1234@localhost/prueba";

router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Postgis & NodeJS'}
  );
});

router.post('/core/login', function(req, res, next) {
  pg.connect(conString, function(err, client, done) {
    if (err) {
      return console.error('error fetching client from pool', err);
    }
    console.log("connected to database");
    client.query('SELECT * FROM dd_user WHERE username = $1 and password = $2 and status = 1', [req.body.user , req.body.pass], function(err, result) {
      done();
      if (err) {
        return console.error('error running query', err);
      }
      res.send(result.rows);
    });
  });
});

router.get('/user', function(req, res, next) {
  pg.connect(conString, function(err, client, done) {
    if (err) {
      return console.error('error fetching client from pool', err);
    }
    console.log("connected to database");
    client.query('SELECT * FROM dd_user', function(err, result) {
      done();
      if (err) {
        return console.error('error running query', err);
      }
      res.send(result.rows);
    });
  });
});

router.get('/user/:id', function(req, res, next) {
  pg.connect(conString, function(err, client, done) {
    if (err) {
      return console.error('error fetching client from pool', err);
    }
    console.log("connected to database");
    client.query('SELECT * FROM dd_user WHERE id = $1', [req.params.id], function(err, result) {
      done();
      if (err) {
        return console.error('error running query', err);
      }
      res.send(result.rows);
    });
  });
});

router.post('/user', function(req, res, next) {
  pg.connect(conString, function(err, client, done) {
    if (err) {
      return console.error('error fetching client from pool', err);
    }
    console.log("connected to database");
    client.query('INSERT INTO dd_user(id,name)VALUES($1, $2) returning *',
        [req.body.id, req.body.name], function(err, result) {
      done();
      if(err) {
        return console.error('error running query', err);
      }
      res.send(result.rows);
    });
  });
});

router.put('/user/:id', function(req, res, next) {
  pg.connect(conString, function(err, client, done) {
    if (err) {
      return console.error('error fetching client from pool', err);
    }
    console.log("connected to database");
    client.query('UPDATE dd_user SET name = $2 WHERE id = $1 returning *', [req.params.id, req.body.name], function(err, result) {
      done();
      if (err) {
        return console.error('error running query', err);
      }
      res.send(result.rows);
    });
  });
});

router.delete('/user/:id', function(req, res, next) {
  pg.connect(conString, function(err, client, done) {
     console.log(conString)
    if (err) {
      return console.error('error fetching client from pool', err);
    }
    console.log("connected to database");
    client.query('DELETE FROM dd_user WHERE id = $1',[req.params.id], function(err, result) {
      done();
      if (err) {
        return console.error('error running query', err);
      }
      res.send(result);
    });
  });
});

module.exports = router;
