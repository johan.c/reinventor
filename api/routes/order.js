var express = require('express');
var router = express.Router();
var order = require('../models/order');
var util = require('../util/util');
var moment = require('moment');

var self = {
  order : {},
  list : []
};

/* GET ORDERS */
router.get(util.api+'/order', function (req, res, next) {
  var query = order.getQuery(req.query);
  util.connection.query(query, function (error, results){
    if (error) {
        console.log(error);
        return res.send(400, {'success':false,'data' : []});
      }else{
        if(results.length > 0){
          var response = self.getList(results);
          return res.send(200, {'success':true,'data' : response});
        }else{
          return res.send({'success':false,'data' : []});
        }
      }
  });
});

/* GET ORDER BY ID */
router.get(util.api+'/order/:id', function (req, res, next) {
  var query = order.getQuery(req.query);
  util.connection.query(query, function (error, results){
    if (error) {
        console.log(error);
        return res.send(400, {'success':false,'data' : []});
      }else{
        if(results.length > 0){
          var response = self.getOrder(results);
          return res.send(200, {'success':true,'data' : response});
        }else{
          return res.send({'success':false,'data' : []});
        }
      }
  });
});

/* INSERT ORDER */
router.post(util.api+'/order', function (req, res, next) {
  var query = order.getQueryInsert();
  var values = [
    [
      req.body.concept_id,
      req.body.employee_id,
      req.body.entity_id
    ]
  ];
  util.connection.query(query, [values], function (error, results) {
    if (error) {
      console.log(error);
      return res.send(400, {'success':false,'data' : []});
    }else{
      if(results){
        return res.send(200, {'success':true,'data' : results});
      }else{
        return res.send({'success':false,'data' : []});
      }
    }
  });
});

/* INSERT ORDER DETAIL */
router.post(util.api+'/order/detail', function (req, res, next) {
  var products = JSON.parse(req.body.products);
  if(products.length > 0){
    var values = [];
    var query = order.getQueryInsertDetail();
    for (var i = 0;i < products.length; i++) {
      var data = [req.body.order_id,products[i].item_id];
      values.push(data);
    }
    util.connection.query(query, [values], function (error, results) {
      if (error) {
        console.log(error);
        return res.send(400, {'success':false,'data' : []});
      }else{
        if(results){
          return res.send(200, {'success':true,'data' : results});
        }else{
          return res.send({'success':false,'data' : []});
        }
      }
    });
  }else{
    return res.send(400, {'success':false,'data' : []});
  }
});

/* UPDATE STATE ORDER */
router.put(util.api+'/order/state/:id', function (req, res, next) {
  var request = {};
  request.state = req.body.state;
  request.id = req.params.id;
  var query = order.getQueryUpdate(request);
  util.connection.query(query, function (error, results) {
    if (error) {
      console.log(error);
      return res.send(400, {'success':false,'data' : []});
    }else{
      if(results){
        return res.send(200, {'success':true,'data' : results});
      }else{
        return res.send({'success':false,'data' : []});
      }
    }
  });
});

self.getList = function(data){
  if(data.length > 0){
    self.list = [];
    for (var i = 0; i < data.length; i++) {
      self.list.push({
        'id' : data[i].id,
        'date_order' : data[i].date_order,
        'reception_date_order' : data[i].reception_date_order,
        'deleted' : data[i].deleted,
        'state_order_id' : data[i].state_order_id,
        'concept_id' : data[i].concept_id,
        'entity_id' : data[i].entity_id,
        'employee_id' : data[i].employee_id,
        'created_at' : data[i].created_at,
        'person_id' : data[i].person_id,
        'person_first_name' : data[i].person_first_name,
        'person_last_name' : data[i].person_last_name,
        'entity_id' : data[i].entity_id,
        'entity_name' : data[i].entity_name,
        'state_id' : data[i].state_id,
        'state_name' : data[i].state_name
      });
    }
  }
  return self.list;
};

//PRODUCTO POR ID
self.getOrder = function(data){
  if(data.length > 0){
    for (var i = 0; i < data.length; i++) {
      self.order = {};
      self.order.id = data[i].id,
      self.order.date_order = data[i].date_order,
      self.order.reception_date_order = data[i].reception_date_order,
      self.order.deleted = data[i].deleted,
      self.order.state_order_id = data[i].state_order_id,
      self.order.concept_id = data[i].concept_id,
      self.order.entity_id = data[i].entity_id,
      self.order.employee_id = data[i].employee_id,
      self.order.created_at = data[i].created_at,
      self.order.person_id = data[i].person_id,
      self.order.person_first_name = data[i].person_first_name,
      self.order.person_last_name = data[i].person_last_name,
      self.order.entity_id = data[i].entity_id,
      self.order.entity_name = data[i].entity_name,
      self.order.state_id = data[i].state_id,
      self.order.state_name = data[i].state_name
    }
  }
  return self.order;
};

/* ========================== ORDER DETAIL ========================================== */

/* GET ORDER DETAIL */
router.get(util.api+'/order/detail', function (req, res, next) {
  var query = order.getQueryDetail(req.query);
  util.connection.query(query, function (error, results){
    if (error) {
        console.log(error);
        return res.send(400, {'success':false,'data' : []});
      }else{
        if(results.length > 0){
          var response = self.getListDetail(results);
          return res.send(200, {'success':true,'data' : response});
        }else{
          return res.send({'success':false,'data' : []});
        }
      }
  });
});

/* GET ORDER DETAIL BY ID */
router.get(util.api+'/order/detail/:id', function (req, res, next) {
  var query = order.getQueryDetail(req.params);
  util.connection.query(query, function (error, results){
    if (error) {
        console.log(error);
        return res.send(400, {'success':false,'data' : []});
      }else{
        if(results.length > 0){
          var response = self.getListDetail(results);
          return res.send(200, {'success':true,'data' : response});
        }else{
          return res.send({'success':false,'data' : []});
        }
      }
  });
});

self.getListDetail = function(data){
  if(data.length > 0){
    self.list = [];
    for (var i = 0; i < data.length; i++) {
      self.list.push({
        'order_id' : data[i].order_id,
        'order_date_order' : data[i].order_date_order,
        'order_reception_date_order' : data[i].order_reception_date_order,
        'order_deleted' : data[i].order_deleted,
        'order_state_order_id' : data[i].order_state_order_id,
        'order_concept_id' : data[i].order_concept_id,
        'order_entity_id' : data[i].order_entity_id,
        'order_employee_id' : data[i].order_employee_id,
        'item_id' : data[i].item_id,
        'item_name' : data[i].item_name,
        'item_category' : data[i].item_category,
        'item_number' : data[i].item_number,
        'item_description' : data[i].item_description,
        'item_cost' : data[i].item_cost,
        'item_unit_price' : data[i].item_unit_price,
        'item_quantity' : data[i].item_quantity,
        'item_level' : data[i].item_level,
        'item_location' : data[i].item_location,
        'item_allow' : data[i].item_allow,
        'item_allow_description' : data[i].item_allow_description,
        'item_serialize' : data[i].item_serialize
      });
    }
  }
  return self.list;
};

/* ================================================================================= */


module.exports = router;