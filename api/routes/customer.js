var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var customer = require('../models/customer');
var api = '/api/v1/c22d6ecd-5711-2efc-2ba1-0d6d42bff682';

var self = {
	customer : {},
	customerList : []
};

connection = mysql.createConnection({
	host : 'localhost',
	user : 'root',
	password : '1234',
	database: 'database'
});

router.get(api+'/customer', function (req, res, next) {
  connection.query(customer.query, function (error, results){
    if (error) {
      return res.send(400, {'success':false,'data' : []});
    }else{
      if(results.length > 0){
        var response = self.getListCustomer(results);
        res.send(200, {'success':true,'data' : response});
      }else{
        return res.send({'success':false,'data' : []});
      }
      return next();
    }
  });
});

/* =================== FUNCIONES DE SERVICIO ============================= */
//LISTADO DE USUARIOS
self.getListCustomer = function(data){
	if(data.length > 0){
		self.customerList = [];
		for (var i = 0; i < data.length; i++) {
			self.customerList.push({
        'person_id' : data[i].person_id,
        'email' : data[i].email,
        'first_name' : data[i].first_name,
        'last_name' : data[i].last_name,
        'phone_number' : data[i].phone_number
			});
		}
	}
	return self.customerList;
};

module.exports = router;
