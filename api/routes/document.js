var express = require('express');
var router = express.Router();
var doc = require('../models/document');
var util = require('../util/util');
var moment = require('moment');

var self = {
  document : {},
  list : []
};

/* LIST DOCUMENT */
router.get(util.api+'/document', function (req, res, next) {

});

/* LIST DOCUMENT BY ORDER */
router.get(util.api+'/document/:order_id', function (req, res, next) {
  var query = doc.getQuery(req.params);
  console.log(query);
  util.connection.query(query, function (error, results) {
    if (error) {
      console.log(error);
      return res.send(400, {'success':false,'data' : []});
    }else{
      if(results.length > 0){
        var response = self.getList(results);
        return res.send(200, {'success':true,'data' : response});
      }else{
        return res.send({'success':false,'data' : []});
      }
    }
  });
});

/* INSERT DOCUMENT */
router.post(util.api+'/document', function (req, res, next) {
  var query = doc.getQueryInsert();
  var values = [
    [
      req.body.number_document,
      req.body.order_id,
      req.body.amount
    ]
  ];
  util.connection.query(query, [values], function (error, results) {
    if (error) {
      console.log(error);
      return res.send(400, {'success':false,'data' : []});
    }else{
      if(results){
        return res.send(200, {'success':true,'data' : results});
      }else{
        return res.send({'success':false,'data' : []});
      }
    }
  });
});

/* METODOS DE INSERCION */
self.getList = function(data){
  if(data.length > 0){
    self.list = [];
    for (var i = 0; i < data.length; i++) {
      self.list.push({
        'id' : data[i].id,
        'number_document' : data[i].number_document,
        'amount' : data[i].amount,
        'created_at' : moment(data[i].created_at).format('MM/DD/YYYY')
      });
    }
  }
  return self.list;
};

module.exports = router;