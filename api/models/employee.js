var express = require('express');
var router = express.Router();

var employee = {
       query : ''
};


employee.getQuery = function(params){
      employee.query =
            ' SELECT '+
            ' c.person_id, '+
            ' o.email, '+
            ' o.first_name, '+
            ' o.last_name ' +
            ' FROM ospos_employees c '+
            ' inner join ospos_people o on c.person_id = o.person_id where deleted = 0 ';
      if(Object.keys(params).length !== 0){
            if(params.hasOwnProperty('id')){
                  employee.query += 'and o.person_id = '+ params.id;
            }
            if(params.hasOwnProperty('name')){
                  employee.query += 'and o.first_name like concat(' + '\'%\'' + ',\''+ params.name +'\',' + '\'%\'' + ') ';
            }
            if(params.hasOwnProperty('limit')){
                  employee.query += 'limit '+ params.limit;
            }
      }
      return employee.query;
};

module.exports = employee;
