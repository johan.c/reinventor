var express = require('express');
var router = express.Router();

var entity = {
	query : ''
};

entity.getQuery = function(params){
	entity.query = 'SELECT ' +
	            'e.ruc,e.name,e.name_short,e.number, '+
	            'e.email,e.phone,e.account,e.contact, '+
	            'e.code,e.flg_suppliers,e.flg_bank, '+
	            'e.flg_salud,e.flg_educ,e.number_length,e.address, '+
	            'e.data,e.status,e.created_at,e.updated_at '+
	            'from ospos_entity e where status = 0 ';

	if(Object.keys(params).length !== 0){
		if(params.hasOwnProperty('name')){
			entity.query += ' and name like concat(' + '\'%\'' + ',\''+ params.name +'\',' + '\'%\'' + ') ';
      	}
		if(params.hasOwnProperty('limit')){
			entity.query += ' limit '+ params.limit;
      	}
	}
	return entity.query;
};

module.exports = entity;