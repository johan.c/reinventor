var express = require('express');
var router = express.Router();

var concept = {
	query : ''
};

concept.getQuery = function(params){
	concept.query = 'SELECT ' +
	            'c.id,c.name,c.count '+
	            'from ospos_concept c where c.deleted = 0 ';

	if(Object.keys(params).length !== 0){
		if(params.hasOwnProperty('id')){
			concept.query += ' and id = ' + params.id;
      	}
		if(params.hasOwnProperty('name')){
			concept.query += ' and name like concat(' + '\'%\'' + ',\''+ params.name +'\',' + '\'%\'' + ') ';
      	}
		if(params.hasOwnProperty('limit')){
			concept.query += ' limit '+ params.limit;
      	}
	}
	return concept.query;
};

module.exports = concept;