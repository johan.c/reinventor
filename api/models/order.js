var express = require('express');
var router = express.Router();

var order = {
	query : '',
	queryDetail : '',
	insert : '',
	update : ''
};

order.getQuery = function(params){
	order.query = '';
	order.query += `select 
					o.id,
					o.date_order,
					o.reception_date_order,
					o.deleted,
					o.state_order_id,
					o.concept_id,
					o.entity_id,
					o.employee_id,
					o.created_at,
					e.person_id as person_id,
					p.first_name as person_first_name,
					p.last_name as person_last_name,
					en.ruc as entity_id,
					en.name as entity_name,
					c.id as state_id,
					c.name as state_name
				from ospos_orders o
				inner join ospos_employees e on o.employee_id = e.person_id
				inner join ospos_people p on e.person_id = p.person_id
				inner join ospos_entity en on en.ruc = o.entity_id
				inner join ospos_code c on c.id = o.state_order_id
				WHERE o.deleted = 0 and o.state_order_id = 10 `;
		if(Object.keys(params).length !== 0){
			if(params.hasOwnProperty('id')){
				order.query += `and o.id = `+ params.id;
			}
		}	
	order.query += ` ORDER BY o.id DESC`;
	return order.query;
};

order.getQueryDetail = function(params){
	order.queryDetail = '';
	order.queryDetail += `select 
					o.id as order_id,
					o.date_order as order_date_order,
					o.reception_date_order as order_reception_date_order,
					o.deleted as order_deleted,
					o.state_order_id as order_state_order_id,
					o.concept_id as order_concept_id,
					o.entity_id as order_entity_id,
					o.employee_id as order_employee_id,
					i.item_id as item_id,
					i.name as item_name,
					i.category as item_category,
					i.item_number as item_number,
					i.description as item_description,
					i.cost_price as item_cost,
					i.unit_price as item_unit_price,
					i.quantity as item_quantity,
					i.reorder_level as item_level,
					i.location as item_location,
					i.item_id as item_allow,
					i.allow_alt_description as item_allow_description,
					i.is_serialized as item_serialize
				from ospos_orders_detail od
				inner join ospos_orders o on od.order_id = o.id
				inner join ospos_items i on od.product_id = i.item_id
				WHERE o.deleted = 0 and o.state_order_id = 10 `;
		if(Object.keys(params).length !== 0){
			if(params.hasOwnProperty('id')){
				order.queryDetail += `and o.id = `+ params.id;
			}
		}
	order.queryDetail += ` ORDER BY o.id DESC`;
	return order.queryDetail;
};

order.getQueryInsert = function(){
	order.insert = `INSERT INTO ospos_orders(concept_id,employee_id,entity_id) VALUES ? `;
	return order.insert;
};

order.getQueryInsertDetail = function(){
	order.insert = `INSERT INTO ospos_orders_detail(order_id,product_id) VALUES ? `;
	return order.insert;
};

order.getQueryUpdate = function(params){
	order.update = `UPDATE ospos_orders set state_order_id = ${params.state} where id = ${params.id};`;
	return order.update;
};

module.exports = order;
