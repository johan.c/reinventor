var express = require('express');
var router = express.Router();

var customer = {
  person_id : 0,
  account_number : "",
  taxable : 0,
  deleted : 0,
  listCustomer : []
};

customer.query =
						'SELECT'+
            ' c.person_id, '+
            ' o.email, '+
            ' o.first_name, '+
            ' o.last_name, '+
            ' o.phone_number, '+
            ' c.account_number, '+
            ' c.taxable '+
            ' FROM ospos_customers c '+
            ' inner join ospos_people o on c.person_id = o.person_id';

module.exports = customer;
