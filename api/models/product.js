var express = require('express');
var router = express.Router();
var product = {
       query : ''
};

product.getQuery = function(params){
       product.query = ' select ' +
              ' name, ' +
              ' category, ' +
              ' supplier_id, ' +
              ' item_number, ' +
              ' description, ' +
              ' cost_price, '       +
              ' unit_price, '              +
              ' quantity, '   +
              ' reorder_level, '+
              ' location, '         +
              ' item_id, ' +
              ' allow_alt_description, ' +
              ' is_serialized, ' +
              ' deleted, ' +
              ' custom1,' +
              ' custom2,' +
              ' custom3, ' +
              ' custom4, ' +
              ' custom5, ' +
              ' custom6, ' +
              ' custom7, ' +
              ' custom8, ' +
              ' custom9, ' +
              ' custom10 ' +
              ' from ospos_items '+
              ' where deleted = 0 ';

       if(Object.keys(params).length !== 0){
              if(params.hasOwnProperty('id')){
                     product.query += 'and item_id = '+ params.id;
              }
              if(params.hasOwnProperty('name')){
                     product.query += 'and name like concat(' + '\'%\'' + ',\''+ params.name +'\',' + '\'%\'' + ') ';
              }
              if(params.hasOwnProperty('limit')){
                     product.query += 'limit '+ params.limit;
              }
       }
       
       return product.query;
};

module.exports = product;